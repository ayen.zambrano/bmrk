%{Phalcon_Version_124eb7eb35b170ac4d15a9fc5b5a7c8c}%
==========================

%{Phalcon_Version_6b52a3fcf09c842cd51b8be95a64756f}%

%{Phalcon_Version_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Version_4b1c6dacfebb6db4f27614af8df45a5f}%

%{Phalcon_Version_2f0f086410bfee6246d626fd0f393838}%

%{Phalcon_Version_0fff482347dfa93ea1d1cecd334cd955}%

%{Phalcon_Version_b24c5da32117a6b290bf685d3da4a49a}%

.. code-block:: php

    <?php

     echo Phalcon\Version::get();





%{Phalcon_Version_9ec983596cc1e3f09b75c143f1ac667b}%

%{Phalcon_Version_d3011e45403732dba4ebfc132d34a031}%

