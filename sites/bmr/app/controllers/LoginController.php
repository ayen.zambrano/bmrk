<?php

use Phalcon\Mvc\Controller;

class LoginController extends Controller
{

	public function initialize(){
		$this->view->setTemplateAfter('loginlayout');
	}

	public function indexAction()
	{
		
		
	}

	public function processAction($username= false, $password = false, $age = 12)
	{
		// echo "processing <br>";
		// echo "Username: ".$username."<br>";
		// echo "Password: ".$password."<br>";
		// echo "Age: ".$age."<br>";
		$this->view->setVar('username', $username);
		$this->view->setVar('password', $password);		
		$this->view->setVar('age', $age);


		//Calling Action Within the Controller (test)
		// $this->dispatcher->forward([
		// 	'controller'=>'login', 
		// 	'action'=>'test'
		// 	]);

	}
	//test action
	// public function testAction(){
	// 	echo "-- Test Action --";
	// }

}
