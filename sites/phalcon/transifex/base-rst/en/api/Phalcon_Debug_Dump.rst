%{Phalcon_Debug_Dump_2ea3739b3cb31bef3c9dce1d9ddce596}%
==============================

%{Phalcon_Debug_Dump_1938f70859fabf5dcd6ef8926c7c3b42}%

%{Phalcon_Debug_Dump_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Debug_Dump_22d0d2f0b72013ffd361137eeade6bfc}%

%{Phalcon_Debug_Dump_47f12ba4885b522cd61ddd4441f9221e}%

%{Phalcon_Debug_Dump_14e39d66029df73f92cc0fd7a44d4677}%

%{Phalcon_Debug_Dump_cdad8d823aa17c6be687a14ac69d9b59}%

.. code-block:: php

    <?php

    echo (new \Phalcon\Debug\Dump())->vars($foo, $bar, $baz);





%{Phalcon_Debug_Dump_c14d43c5f9a3e0e641fb45725500cf76}%

%{Phalcon_Debug_Dump_df85051adc722c096dd6efb886e4487d}%

.. code-block:: php

    <?php

    echo (new \Phalcon\Debug\Dump())->dump($foo, "foo");





%{Phalcon_Debug_Dump_01f63c8412a9a98b5afe831cb17d6a18}%

%{Phalcon_Debug_Dump_1a57a898c71fc6314a7369bcbbb3b2c4}%

%{Phalcon_Debug_Dump_34707840000fc66e9ccdbd9ef94a9f86}%

%{Phalcon_Debug_Dump_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Debug_Dump_8fb225ff4c8f346e54ef944b8bb8b0d0}%

%{Phalcon_Debug_Dump_99879e9966f4e26c755fc325e6b0db05}%

