%{Phalcon_Translate_Adapter_NativeArray_8eb279b694c6f5dc6df12fbdd495a8bc}%
==================================================

%{Phalcon_Translate_Adapter_NativeArray_367c34ed4758b1d5b6d563bc40968278|:doc:`Phalcon\\Translate\\Adapter <Phalcon_Translate_Adapter>`}%

%{Phalcon_Translate_Adapter_NativeArray_6d0174ab99416d9dc2bf7086acecdb21|:doc:`Phalcon\\Translate\\AdapterInterface <Phalcon_Translate_AdapterInterface>`}%

%{Phalcon_Translate_Adapter_NativeArray_3b02c9834e641d2af0befb6bc91b8ded}%

%{Phalcon_Translate_Adapter_NativeArray_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Translate_Adapter_NativeArray_b6d2e5c73a33db2807abbc44bda3adb4}%

%{Phalcon_Translate_Adapter_NativeArray_00037e44555519ab4fc6ede202e42ca3}%

%{Phalcon_Translate_Adapter_NativeArray_7dbe2da1ab670957400caecff9ff6134}%

%{Phalcon_Translate_Adapter_NativeArray_0ba1ff28ab4462b8777b4a79730a5f2f}%

%{Phalcon_Translate_Adapter_NativeArray_c4e91b22ddc993bb60456a9cfd98e014}%

%{Phalcon_Translate_Adapter_NativeArray_575acaa8817bbfb4515635eabc9edba6}%

%{Phalcon_Translate_Adapter_NativeArray_8103d9eed42a9efbe0899c3b11e9470c}%

%{Phalcon_Translate_Adapter_NativeArray_57564cfd8a909689ba1a1aebc51ac3fe}%

%{Phalcon_Translate_Adapter_NativeArray_c513482ed66ac2dc88481f986be0c1c7}%

%{Phalcon_Translate_Adapter_NativeArray_1e3a141450a8604091c3ea72717e1308}%

%{Phalcon_Translate_Adapter_NativeArray_e023e2ca977bfb54ea2c6ca351fafdff}%

%{Phalcon_Translate_Adapter_NativeArray_5cb60ce8ee6024abf4a718ea345459b4}%

%{Phalcon_Translate_Adapter_NativeArray_faa561a6826fd147e0db37e3670f3d2e}%

%{Phalcon_Translate_Adapter_NativeArray_f8bcc688b7f6647863abfe57824fd9c0}%

%{Phalcon_Translate_Adapter_NativeArray_2a3234025afaa5e9773b5e0e33102e39}%

%{Phalcon_Translate_Adapter_NativeArray_0ba1ff28ab4462b8777b4a79730a5f2f}%

