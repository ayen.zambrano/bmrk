%{Phalcon_Mvc_Collection_ManagerInterface_70412c960b33e9a53931c2817dff5eb7}%
========================================================

%{Phalcon_Mvc_Collection_ManagerInterface_7d1d7e0fd6ead3706dc38d149b8e7e67}%

%{Phalcon_Mvc_Collection_ManagerInterface_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Collection_ManagerInterface_46301c202e3f65e26284ee9354b19968|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Collection_ManagerInterface_6f33293960d052d3768dc3059dc5491b}%

%{Phalcon_Mvc_Collection_ManagerInterface_8f63b4b0a239d2f0eed027a0f8d6f283|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_ManagerInterface_3864b73bbb521140066799837212cb9a}%

%{Phalcon_Mvc_Collection_ManagerInterface_e6588f7978f90e91cf85eb4906edec12|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_ManagerInterface_4b067296c7c4bfbd5a48116388c19b89}%

%{Phalcon_Mvc_Collection_ManagerInterface_431711e58ee9b10039c82a4c73e5457b}%

%{Phalcon_Mvc_Collection_ManagerInterface_c52ee46fe0a73bff1a27e102d1c0f49c}%

%{Phalcon_Mvc_Collection_ManagerInterface_c61884a5ec30137ac1a44500c27a101e|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_ManagerInterface_11886d1443ed07ef878302e83d33dd30}%

%{Phalcon_Mvc_Collection_ManagerInterface_fbd4864c616712420ba8114f84d19dcb|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_ManagerInterface_2fa4a6e0306b53b434e0791ac72cc09d}%

%{Phalcon_Mvc_Collection_ManagerInterface_15336d97725b4576e329ad9579887927|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_ManagerInterface_7c8f580e0d9b3029bc0066d6c2445f51}%

%{Phalcon_Mvc_Collection_ManagerInterface_a886d11fba12990f5cac106d7852732d|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_ManagerInterface_a5a33de749796a6093af6715e763ede1}%

%{Phalcon_Mvc_Collection_ManagerInterface_bf303e083b422105aa24a26ae4c70b22|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_ManagerInterface_2bd73e2486a34dc90c75dfa92142b08c}%

%{Phalcon_Mvc_Collection_ManagerInterface_4a8ec179d04189b7bbd82a3dc770b97b|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_ManagerInterface_5ab646fcc0177d33b88cec54073f262e}%

