%{Phalcon_Assets_Manager_66188de56ec2634af83d1f88f064ab13}%
==================================

%{Phalcon_Assets_Manager_b3edb53b89ea8fd75c7f6caacc96b255}%

%{Phalcon_Assets_Manager_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Assets_Manager_38d168e0be68a8fb5da8d35b168503c0}%

%{Phalcon_Assets_Manager_c6f981189e440a753a63cf64231c08c7}%

%{Phalcon_Assets_Manager_d9eef5f00e112670e894f3cb120d1000|:doc:`Phalcon\\Assets\\Manager <Phalcon_Assets_Manager>`}%

%{Phalcon_Assets_Manager_5b4b9d168fa27fd5aa0009067ccdfb81}%

%{Phalcon_Assets_Manager_8cd706d17906efae1c72cecaa708884f}%

%{Phalcon_Assets_Manager_de67e95108506cf28541e4a9ffbde17f}%

%{Phalcon_Assets_Manager_7adc483afee8c73d60a8d2a069b9d9af|:doc:`Phalcon\\Assets\\Manager <Phalcon_Assets_Manager>`}%

%{Phalcon_Assets_Manager_c92f37326c91857e6adda5fee5192162}%

%{Phalcon_Assets_Manager_c37aa66440e224359e3b3c13b767debd|:doc:`Phalcon\\Assets\\Manager <Phalcon_Assets_Manager>`}%

%{Phalcon_Assets_Manager_ba6e80d0ad9f149a93c0c3f6c970b50f}%

.. code-block:: php

    <?php

    $assets->addCss('css/bootstrap.css');
    $assets->addCss('http://bootstrap.my-cdn.com/style.css', false);





%{Phalcon_Assets_Manager_446c630368afd4b111b80cc289b598be|:doc:`Phalcon\\Assets\\Manager <Phalcon_Assets_Manager>`}%

%{Phalcon_Assets_Manager_82661fd5749c0192b1772d633dcc88e6}%

.. code-block:: php

    <?php

    $assets->addJs('scripts/jquery.js');
    $assets->addJs('http://jquery.my-cdn.com/jquery.js', true);





%{Phalcon_Assets_Manager_9557ba6b4bb0269325cc15d0483ea5e5|:doc:`Phalcon\\Assets\\Manager <Phalcon_Assets_Manager>`|:doc:`Phalcon\\Assets\\Resource <Phalcon_Assets_Resource>`}%

%{Phalcon_Assets_Manager_15b29a025875ea507ee3a6fb2251a839}%

.. code-block:: php

    <?php

    $assets->addResourceByType('css', new Phalcon\Assets\Resource\Css('css/style.css'));





%{Phalcon_Assets_Manager_6a0a0bbd1d8cdbb73d4d345f66d7c368|:doc:`Phalcon\\Assets\\Manager <Phalcon_Assets_Manager>`|:doc:`Phalcon\\Assets\\Resource <Phalcon_Assets_Resource>`}%

%{Phalcon_Assets_Manager_a11c0ca6003e2de535041be67d5d941a}%

.. code-block:: php

    <?php

     $assets->addResource(new Phalcon\Assets\Resource('css', 'css/style.css'));





%{Phalcon_Assets_Manager_897eaf84e43b0cec3210c0f305e84b51|:doc:`Phalcon\\Assets\\Manager <Phalcon_Assets_Manager>`|:doc:`Phalcon\\Assets\\Collection <Phalcon_Assets_Collection>`}%

%{Phalcon_Assets_Manager_5de32e618c35a2face292c6e1df49b80}%

.. code-block:: php

    <?php

     $assets->set('js', $collection);





%{Phalcon_Assets_Manager_9d6358d035324f4da78ccac8f802ce65|:doc:`Phalcon\\Assets\\Collection <Phalcon_Assets_Collection>`}%

%{Phalcon_Assets_Manager_2102f30e1b929a632a41e50fa220f8d5}%

.. code-block:: php

    <?php

     $scripts = $assets->get('js');





%{Phalcon_Assets_Manager_ef0c61d98e9e6f131f51d6c4c438dc48|:doc:`Phalcon\\Assets\\Collection <Phalcon_Assets_Collection>`}%

%{Phalcon_Assets_Manager_f03a4a4ce895cc82aee999c083ea475f}%

%{Phalcon_Assets_Manager_9d899df6fedce2393a05a22333ba4b7d|:doc:`Phalcon\\Assets\\Collection <Phalcon_Assets_Collection>`}%

%{Phalcon_Assets_Manager_420acf3edf81ad3c8ed00c77f4781f1f}%

%{Phalcon_Assets_Manager_cfaab7471f7354594960302b865641d3|:doc:`Phalcon\\Assets\\Collection <Phalcon_Assets_Collection>`}%

%{Phalcon_Assets_Manager_6950b4e5da4fb56ff56bbfbffffcfbed}%

%{Phalcon_Assets_Manager_c89160809fff43145255bca6fb9d064d|:doc:`Phalcon\\Assets\\Collection <Phalcon_Assets_Collection>`}%

%{Phalcon_Assets_Manager_5c12703bd1988439376381c23db61997}%

%{Phalcon_Assets_Manager_919f95f71e9a7b74a8431ad3d48cb82f}%

%{Phalcon_Assets_Manager_bda64bc144843a337c14e6030514a60a}%

%{Phalcon_Assets_Manager_349e008514a8dc0b704d3d0334b96ce3}%

%{Phalcon_Assets_Manager_ab0e1f4de6456a8ece79c0f401177d8f}%

