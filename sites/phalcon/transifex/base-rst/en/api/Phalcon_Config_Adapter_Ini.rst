%{Phalcon_Config_Adapter_Ini_6638c0efb2f3eefac40f4a0eb51fdec2}%
=======================================

%{Phalcon_Config_Adapter_Ini_ea6c93ca1a7d7149d6682d7966a401a4|:doc:`Phalcon\\Config <Phalcon_Config>`}%

%{Phalcon_Config_Adapter_Ini_fca1ea0b1fe3f9769124d410df005401}%

%{Phalcon_Config_Adapter_Ini_d46c6032e6e408f95fb14c8a6ca3da26}%

.. code-block:: ini

    <?php

    [database]
    adapter = Mysql
    host = localhost
    username = scott
    password = cheetah
    dbname = test_db
    
    [phalcon]
    controllersDir = "../app/controllers/"
    modelsDir = "../app/models/"
    viewsDir = "../app/views/"

  You can read it as follows:  

.. code-block:: php

    <?php

    $config = new Phalcon\Config\Adapter\Ini("path/config.ini");
    echo $config->phalcon->controllersDir;
    echo $config->database->username;




%{Phalcon_Config_Adapter_Ini_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Config_Adapter_Ini_c044a39a61c4cca5faedb1dcc3867532}%

%{Phalcon_Config_Adapter_Ini_0e78362993bd19db34e6a888e2cf7dab}%

%{Phalcon_Config_Adapter_Ini_ad184d301499efce8f3964ca6dc4bb3a}%

%{Phalcon_Config_Adapter_Ini_cef9ef423d3226f1df89e6c695359390}%

.. code-block:: php

    <?php

     var_dump(isset($config['database']));





%{Phalcon_Config_Adapter_Ini_3712567de59f17bfbe84e4a63effe296}%

%{Phalcon_Config_Adapter_Ini_0b8c0846e2f4b6b32161721525431ec3}%

.. code-block:: php

    <?php

     echo $config->get('controllersDir', '../app/controllers/');





%{Phalcon_Config_Adapter_Ini_250aecef2df720bcebe5f7cb2ae8bd78}%

%{Phalcon_Config_Adapter_Ini_cb9d856c70df42bb2d3346be63f239cc}%

.. code-block:: php

    <?php

     print_r($config['database']);





%{Phalcon_Config_Adapter_Ini_f2d8b16bf9da301de67f85a740661e95}%

%{Phalcon_Config_Adapter_Ini_a8bce44be354ca47d470b0631b08fccf}%

.. code-block:: php

    <?php

     $config['database'] = array('type' => 'Sqlite');





%{Phalcon_Config_Adapter_Ini_29871184433a14db720da0828895a713}%

%{Phalcon_Config_Adapter_Ini_bee829db6cea0d7a64fd97770ff67610}%

.. code-block:: php

    <?php

     unset($config['database']);





%{Phalcon_Config_Adapter_Ini_8f080354fc2bf1de28b68db6d7fd3c80|:doc:`Phalcon\\Config <Phalcon_Config>`}%

%{Phalcon_Config_Adapter_Ini_cb6ab50e7931dd69b9e01ede416ae818}%

.. code-block:: php

    <?php

    $appConfig = new Phalcon\Config(array('database' => array('host' => 'localhost')));
    $globalConfig->merge($config2);





%{Phalcon_Config_Adapter_Ini_82d6f2943519bac0b9df5c24b7205c04}%

%{Phalcon_Config_Adapter_Ini_12680a2b6379e22d5147df5a8a4f0267}%

.. code-block:: php

    <?php

    print_r($config->toArray());





%{Phalcon_Config_Adapter_Ini_807079f12d6081992cea44ccadb3a6fb}%

%{Phalcon_Config_Adapter_Ini_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Ini_8a1c48ad387accfb23e5b14ab8a2204b}%

%{Phalcon_Config_Adapter_Ini_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Ini_68f9c10a09334876e0988c8d46977b50|:doc:`Phalcon\\Config <Phalcon_Config>`}%

%{Phalcon_Config_Adapter_Ini_b23e852f6995564547321fb333c04912}%

%{Phalcon_Config_Adapter_Ini_17ab47f2caca58e3466bc9754b2378df}%

%{Phalcon_Config_Adapter_Ini_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Ini_99bec0673999e64b60b71ba5935a345b}%

%{Phalcon_Config_Adapter_Ini_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Ini_6b74253a88eb7ec2b979fd16b3c976ba}%

%{Phalcon_Config_Adapter_Ini_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Ini_45106ea4943377cec724075ab9030769}%

%{Phalcon_Config_Adapter_Ini_68cf0a3bff1e41b907cf955f570c5249}%

