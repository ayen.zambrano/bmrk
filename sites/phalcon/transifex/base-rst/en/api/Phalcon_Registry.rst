%{Phalcon_Registry_2a365b481312d43bcb19a92f324d3368}%
=================================

%{Phalcon_Registry_ce932dea8746c4e501786106b1df24bd}%

%{Phalcon_Registry_89e4f5c6ed0aa0af63e0846accc3b1a3}%

.. code-block:: php

    <?php

     	$registry = new \Phalcon\Registry();
    
     	// {%Phalcon_Registry_2ec4d13e8ea24ff0da7a4ddf4f7c8324%}
     	$registry->something = 'something';
     	// {%Phalcon_Registry_e81c4e4f2b7b93b481e13a8553c2ae1b%}
     	$registry['something'] = 'something';
    
     	// {%Phalcon_Registry_39c85def18dd9728cfdf6106a52bfc86%}
     	$value = $registry->something;
     	// {%Phalcon_Registry_e81c4e4f2b7b93b481e13a8553c2ae1b%}
     	$value = $registry['something'];
    
     	// {%Phalcon_Registry_693e064ac3e527451e97dfbd7c542e66%}
     	$exists = isset($registry->something);
     	// {%Phalcon_Registry_e81c4e4f2b7b93b481e13a8553c2ae1b%}
     	$exists = isset($registry['something']);
    
     	// {%Phalcon_Registry_c9f88e098f6fe4e4e112eeb05ccb9671%}
     	unset($registry->something);
     	// {%Phalcon_Registry_e81c4e4f2b7b93b481e13a8553c2ae1b%}
     	unset($registry['something']);

  In addition to ArrayAccess, Phalcon\\Registry also implements Countable (count($registry) will return the number of elements in the registry), Serializable and Iterator (you can iterate over the registry using a foreach loop) interfaces. For PHP 5.4 and higher, JsonSerializable interface is implemented.  Phalcon\\Registry is very fast (it is typically faster than any userspace implementation of the registry); however, this comes at a price: Phalcon\\Registry is a final class and cannot be inherited from.  Though Phalcon\\Registry exposes methods like __get(), offsetGet(), count() etc, it is not recommended to invoke them manually (these methods exist mainly to match the interfaces the registry implements): $registry->__get('property') is several times slower than $registry->property.  Internally all the magic methods (and interfaces except JsonSerializable) are implemented using object handlers or similar techniques: this allows to bypass relatively slow method calls.



%{Phalcon_Registry_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Registry_e53e39ef9124fdaf502df86665d52fe5}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_0014d861097168e8f304d9a82973633a}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_f8457b6e6bc7c32562b3138b58b22342}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_19ed33479cdae693d8ef869f55426eb1}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_1be4757a623da259c04b1ae41f154e3a}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_dac5cec50eda033a74b25dcc1b83e1ba}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_f5a043ff4cdbf7e308abd0d2e36ff6cb}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_2f89883b4ea5702517b8537bed8db873}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_3644e01a19440baa19219140adc68825}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_9ab68bc06a3129747a4a59466efb3f27}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_6c2241241fc0a0674e8a0eec38b5c86c}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_3938d25e02dd9fb8fd9858c4e4d8fe14}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_2b503cefe62dd539e45818e4fd0f9833}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_d07d7e709333e5a70beedd81cc47a746}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_f0fbc0c23a06e0aa387c9388d3a86026}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_054abe3f992ce6c19670b62995ff8f22}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_4d04faa494b108880e51b5394a26cea5}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_e299d36d06648957330fcf22840f28f9}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Registry_f1fd6c91fa6fc886ee543a1ea06b0897}%

%{Phalcon_Registry_68cf0a3bff1e41b907cf955f570c5249}%

