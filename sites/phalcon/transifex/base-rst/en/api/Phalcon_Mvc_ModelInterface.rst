%{Phalcon_Mvc_ModelInterface_e935264157fc69745dc31c7ce3ee9f9d}%
==========================================

%{Phalcon_Mvc_ModelInterface_e9a707063d5cdbf298aea03088dbe69b}%

%{Phalcon_Mvc_ModelInterface_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_ModelInterface_068c7405a1ff56c669c0d9568661ca35|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`|:doc:`Phalcon\\Mvc\\Model\\TransactionInterface <Phalcon_Mvc_Model_TransactionInterface>`}%

%{Phalcon_Mvc_ModelInterface_4e5029c34ffe9fe15c1bae9284f01438}%

%{Phalcon_Mvc_ModelInterface_6147dc50640841ed7818ee77c03fac11}%

%{Phalcon_Mvc_ModelInterface_dcd1c671d82c9ca4d9507ecb1b9297d1}%

%{Phalcon_Mvc_ModelInterface_4761ab1f8ce3899c4f2439c0bfd1717f}%

%{Phalcon_Mvc_ModelInterface_eca5146574824fc8e6d76afeba26a63d}%

%{Phalcon_Mvc_ModelInterface_29faaacf9b77f72c2b78acb3f0189d9b}%

%{Phalcon_Mvc_ModelInterface_275e96afea5ebdfbbdc209fc410498ac}%

%{Phalcon_Mvc_ModelInterface_c8fd648fe903aa197815da65bd696170}%

%{Phalcon_Mvc_ModelInterface_49d496919ccf3aef84738bd59c2a3ff6}%

%{Phalcon_Mvc_ModelInterface_429d7942eb6878f317fe7eb0ef38642b}%

%{Phalcon_Mvc_ModelInterface_85fc00b3bb2282489e524dc5792221c9}%

%{Phalcon_Mvc_ModelInterface_16a18c28eb1d03daa7c6872421fd9731}%

%{Phalcon_Mvc_ModelInterface_da63cce09c70c77f4f97f3f05dc3254e}%

%{Phalcon_Mvc_ModelInterface_350ccc545c65f725913076b0a888763f}%

%{Phalcon_Mvc_ModelInterface_f63025282cd21cea77668141d1992344}%

%{Phalcon_Mvc_ModelInterface_de35c449835d6879bcbfed27887d8e28|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`}%

%{Phalcon_Mvc_ModelInterface_3c12f2abcaa4fa0e6b5269fd8b367019}%

%{Phalcon_Mvc_ModelInterface_35ce2b9f7f1e1617622931135cfeb32c|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`}%

%{Phalcon_Mvc_ModelInterface_3c12f2abcaa4fa0e6b5269fd8b367019}%

%{Phalcon_Mvc_ModelInterface_3634832d6bbd912d44166d2f2142c212|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_ModelInterface_d4469ba918fbd32b6ec2b6a346017fc2}%

%{Phalcon_Mvc_ModelInterface_e30c9836ae9ad3a1924d53cf9532eb28|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_ModelInterface_f1d54db4f2526da20a77ee0ce2f3a39f}%

%{Phalcon_Mvc_ModelInterface_91d171b84981bc578f4dbdb12f5a8bd8|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_ModelInterface_f1d54db4f2526da20a77ee0ce2f3a39f}%

%{Phalcon_Mvc_ModelInterface_06dc5aea97f2b957412b90b15fa11109}%

%{Phalcon_Mvc_ModelInterface_5fbed31187debab89f0ebab12af93c3a}%

%{Phalcon_Mvc_ModelInterface_ea7181c11e659b4edee5a7c7bcf2cd4c|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`}%

%{Phalcon_Mvc_ModelInterface_5a1865ab17e977b53c4c4f99480ede1d}%

%{Phalcon_Mvc_ModelInterface_70cb14a18f5fd3836be66d619c0e2436|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_ModelInterface_ba5c6580c11c5a24caec7a5a0c07415a}%

%{Phalcon_Mvc_ModelInterface_cd0842fea4b817b013fccd9e9cccd614|:doc:`Phalcon\\Mvc\\Model\\CriteriaInterface <Phalcon_Mvc_Model_CriteriaInterface>`|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_ModelInterface_55c9fd44b47633fc584e6f591ff693c2}%

%{Phalcon_Mvc_ModelInterface_f6d8a538b7e9fb757c8e1c0befe54a31}%

%{Phalcon_Mvc_ModelInterface_f7223686d443b7c9a1e05783aff820b7}%

%{Phalcon_Mvc_ModelInterface_ebd74af38b08bec9d50552318b387820}%

%{Phalcon_Mvc_ModelInterface_e67d63562222d390f2ebb5d4d322bda2}%

%{Phalcon_Mvc_ModelInterface_3225074bab19d848c0f68167e87e0caa}%

%{Phalcon_Mvc_ModelInterface_59c547da664c6ddcfd9d09f9a0cf0ce8}%

%{Phalcon_Mvc_ModelInterface_75ded1bf1c4cf02196f72671e795cab6}%

%{Phalcon_Mvc_ModelInterface_aa80298ba41b9f1dc12f6ba2586ab155}%

%{Phalcon_Mvc_ModelInterface_6906a7b3f8f83d43f9fb4f8555a854f4}%

%{Phalcon_Mvc_ModelInterface_ae51bedfd999108176ede0e4a4408e7c}%

%{Phalcon_Mvc_ModelInterface_ea0614c44649d07f3316b1d7f360a5c9}%

%{Phalcon_Mvc_ModelInterface_f99bf1ab50d5a2b74a0dac8fab33b149}%

%{Phalcon_Mvc_ModelInterface_df86108661053542531e78e5c7166cd6}%

%{Phalcon_Mvc_ModelInterface_5e15fe75536353a7b09e6220bf5e6b7f}%

%{Phalcon_Mvc_ModelInterface_dfd0f51f263136160a7ab8839fec756b|:doc:`Phalcon\\Mvc\\Model\\MessageInterface <Phalcon_Mvc_Model_MessageInterface>`}%

%{Phalcon_Mvc_ModelInterface_cf3ee27f3b0653e066ece2f5515a3705}%

%{Phalcon_Mvc_ModelInterface_8297a20eed5081eee54f08bff2c2d6b2}%

%{Phalcon_Mvc_ModelInterface_1fa7cfb1ecc0ee4d1e9213b7ae524795}%

%{Phalcon_Mvc_ModelInterface_2195f9b2d4faf575c1aa0c8fe5db4d60|:doc:`Phalcon\\Mvc\\Model\\MessageInterface <Phalcon_Mvc_Model_MessageInterface>`}%

%{Phalcon_Mvc_ModelInterface_8e5c3d322cb3efd907589c23f073a7dc}%

%{Phalcon_Mvc_ModelInterface_e779c72c6018b2dc9246f48e50674a6c}%

%{Phalcon_Mvc_ModelInterface_1ef5533328aef587c2f2577a8f93ce5c}%

%{Phalcon_Mvc_ModelInterface_9da14c6b2866abe24bc31284299aea05}%

%{Phalcon_Mvc_ModelInterface_aaa02ba44bba13404089535a6c9d8235}%

%{Phalcon_Mvc_ModelInterface_7c5cd9c3c459f65382fecbb8517a7a8d}%

%{Phalcon_Mvc_ModelInterface_e2e31db741b08665d346bec3f7eb6016}%

%{Phalcon_Mvc_ModelInterface_927a18f6c37c6b39383835d99daa9284}%

%{Phalcon_Mvc_ModelInterface_a203f5a265c355b4566c92e47553c360}%

%{Phalcon_Mvc_ModelInterface_d4bacbef5e1c8a276a76367bf0592b44}%

%{Phalcon_Mvc_ModelInterface_00a466a9565b33ee8d53cf519f96cb67}%

%{Phalcon_Mvc_ModelInterface_f9038927f5c97dc7b42b2772b9892df9}%

%{Phalcon_Mvc_ModelInterface_fc91a9f199f437cd77d804b9dd6a51cc}%

%{Phalcon_Mvc_ModelInterface_3979a1bdb59909642c36ce1a8d2b4327}%

%{Phalcon_Mvc_ModelInterface_90c310a37472da42bc3397eb37ea03fe}%

%{Phalcon_Mvc_ModelInterface_f007ed6f5128d58fcc29f6106560f04f}%

%{Phalcon_Mvc_ModelInterface_da1d780364506f576c1897e53fd04d3f}%

%{Phalcon_Mvc_ModelInterface_268d624dbc625b180d4c3eafb791eac0|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`}%

%{Phalcon_Mvc_ModelInterface_f580ba63e18a688de4c42b53a8da76bd}%

