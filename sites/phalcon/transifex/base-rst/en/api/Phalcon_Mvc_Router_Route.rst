%{Phalcon_Mvc_Router_Route_0cd803073ff29c58a4a04345fad08579}%
=====================================

%{Phalcon_Mvc_Router_Route_63d5a061c4d46af666a8351e47838f23|:doc:`Phalcon\\Mvc\\Router\\RouteInterface <Phalcon_Mvc_Router_RouteInterface>`}%

%{Phalcon_Mvc_Router_Route_c0815197e1df4e79e25edd90a3427f87}%

%{Phalcon_Mvc_Router_Route_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Router_Route_06a83132bea9b905a9a0ae3f8985c174}%

%{Phalcon_Mvc_Router_Route_cfd17a3b77599c2730b9b9a0cc12b66d}%

%{Phalcon_Mvc_Router_Route_bd375ef859fd767e3d6ffcd9fdb7694e}%

%{Phalcon_Mvc_Router_Route_5afe4c5dd538ef462618fcc486d81492}%

%{Phalcon_Mvc_Router_Route_d050feae30ca19f73efa04b32e7abb4b|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Route_d3e77b4b3c8f76c19133733b46da1d8d}%

.. code-block:: php

    <?php

     $route->via('GET');
     $route->via(array('GET', 'POST'));





%{Phalcon_Mvc_Router_Route_012e97c7782ccbd3e3d12d4f98f057b0}%

%{Phalcon_Mvc_Router_Route_649c09ba3260b93c870dd02429bfdfb9}%

%{Phalcon_Mvc_Router_Route_bad4450b78127e6c6b52db7413174386}%

%{Phalcon_Mvc_Router_Route_feb61e4b8e658048c2c39a1d8e06e054}%

%{Phalcon_Mvc_Router_Route_e89719dcfd662d9dab881162a6933980|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Route_1e974d8a52139a09e74244a26c938090}%

.. code-block:: php

    <?php

     $router->add('/about', array(
         'controller' => 'about'
     ))->setName('about');





%{Phalcon_Mvc_Router_Route_14a3112c35c8b03a10e9c017799e759a|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Route_16ad44e06fe31aa56549508359b346d7}%

%{Phalcon_Mvc_Router_Route_f6a0c7ec7595dfcb6d4af2d434e48042}%

%{Phalcon_Mvc_Router_Route_a126e052d30d594ef94b18c595157d91}%

%{Phalcon_Mvc_Router_Route_a4b62383bed5d387a53c4aeb7f57c61f}%

%{Phalcon_Mvc_Router_Route_e4f8f7099cc6572bad7c9a8dc0d19429}%

%{Phalcon_Mvc_Router_Route_3d5c29f58f3fba2b71fb67729a7240fd}%

%{Phalcon_Mvc_Router_Route_5a700336a7b158de35e3be1ef132c97e}%

%{Phalcon_Mvc_Router_Route_2e5c9cc0c2703e63858e508a57fd1a10}%

%{Phalcon_Mvc_Router_Route_502ecd3c6de2eb7c637a9bfd09e20c80}%

%{Phalcon_Mvc_Router_Route_59056adaca64fc7a900dafd935cd6107}%

%{Phalcon_Mvc_Router_Route_90e32d1d1e114cb8a5269ef2064b8d69}%

%{Phalcon_Mvc_Router_Route_a8006fe2995966682d59f1c52240b7ca}%

%{Phalcon_Mvc_Router_Route_ba9568021c378e6950b54e9517f06ffc}%

%{Phalcon_Mvc_Router_Route_6e806150f5c9b3c53378d89dfe9d4e27|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Route_c5e69f30a715fdda1f9e1a94e49ccd20}%

.. code-block:: php

    <?php

     $route->setHttpMethods('GET');
     $route->setHttpMethods(array('GET', 'POST'));





%{Phalcon_Mvc_Router_Route_c5a370e05ce402543b33b20a664762b0}%

%{Phalcon_Mvc_Router_Route_3b8e11c325702506db0ae1cb8e488ca4}%

%{Phalcon_Mvc_Router_Route_d001bc8c3373419d0be335a52005116a|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Route_84cc1c2151d359a316c9ba1af4468609}%

.. code-block:: php

    <?php

     $route->setHostname('localhost');





%{Phalcon_Mvc_Router_Route_5584a065a26dfa13a62886d261bb78ec}%

%{Phalcon_Mvc_Router_Route_9345ed7771313beb1706cacdaf816671}%

%{Phalcon_Mvc_Router_Route_9f4ab1e9e835495b5ae1bdaa3c86d73c|:doc:`Phalcon\\Mvc\\Router\\Group <Phalcon_Mvc_Router_Group>`}%

%{Phalcon_Mvc_Router_Route_748931ba64e7b80b4c8973cbf30adaef}%

%{Phalcon_Mvc_Router_Route_bf5219ab99ebfca87140deecc23e1542|:doc:`Phalcon\\Mvc\\Router\\Group <Phalcon_Mvc_Router_Group>`}%

%{Phalcon_Mvc_Router_Route_008a7d9aff3f849709ee5ffbb01f3823}%

%{Phalcon_Mvc_Router_Route_5c06178e46be1033ac16569edd438541|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Route_90d6720b705e2eca7bab333ee9b666d3}%

%{Phalcon_Mvc_Router_Route_b6ea498174ac1b895dc3c75f0452e85f}%

%{Phalcon_Mvc_Router_Route_bd61c1d6f9e567f648af334a5d2cd919}%

%{Phalcon_Mvc_Router_Route_2ce62b51773f221fe3c33e72402b4938}%

%{Phalcon_Mvc_Router_Route_e30a39f1d486bf63c211bd55585ba8ca}%

