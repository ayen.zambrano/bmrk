%{Phalcon_Mvc_Model_a894997ad1ab7e651c9dd8a364144ac5}%
======================================

%{Phalcon_Mvc_Model_4e697b02a9f7cc9b2bcd83cfe8fbe6c8|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`|:doc:`Phalcon\\Mvc\\Model\\ResultInterface <Phalcon_Mvc_Model_ResultInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_Model_54a3f0b3aa84d19d4a1473a6833103a8}%

.. code-block:: php

    <?php

     $robot = new Robots();
     $robot->type = 'mechanical';
     $robot->name = 'Astro Boy';
     $robot->year = 1952;
     if ($robot->save() == false) {
      echo "Umh, We can store robots: ";
      foreach ($robot->getMessages() as $message) {
        echo $message;
      }
     } else {
      echo "Great, a new robot was saved successfully!";
     }




%{Phalcon_Mvc_Model_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Mvc_Model_60fc354d019cc69a556db79952e9fe27}%

%{Phalcon_Mvc_Model_65a0d3777c5a9c21e0a287986c8fc77c}%

%{Phalcon_Mvc_Model_b4b6325a6a4b6248d3b7a4131c13b6bf}%

%{Phalcon_Mvc_Model_55da66508c354dcc0293409f7bed55ef}%

%{Phalcon_Mvc_Model_351ffea01d0395812bd330300fd71a1b}%

%{Phalcon_Mvc_Model_98424da4c8151d2607959f22389266d1}%

%{Phalcon_Mvc_Model_bbd4900762818730c476a929086d7b4f}%

%{Phalcon_Mvc_Model_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_767f6b96c206c1336b09ad20f2f4102a|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`|:doc:`Phalcon\\Mvc\\Model\\ManagerInterface <Phalcon_Mvc_Model_ManagerInterface>`}%

%{Phalcon_Mvc_Model_c3c0ba014162bf7967f764fab4b191c3}%

%{Phalcon_Mvc_Model_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_992eb21f94076f486939b432849d799c}%

%{Phalcon_Mvc_Model_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_1bac54934bec20940b708d05267cfd49}%

%{Phalcon_Mvc_Model_68dfba0a03678d8fd7215e65556aa6fc}%

%{Phalcon_Mvc_Model_5abe60a2267d1a558edff05114e36ed6}%

%{Phalcon_Mvc_Model_83e406da5dbad46d8845e952a7c95908|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Model_3e152c1d4a8e1e44eb8e734d54a06599}%

%{Phalcon_Mvc_Model_ef2378790af2c25b862a2e14c26e0ada|:doc:`Phalcon\\Mvc\\Model\\MetaDataInterface <Phalcon_Mvc_Model_MetaDataInterface>`}%

%{Phalcon_Mvc_Model_f92d20395997c73f60e9991f6ddfdd41}%

%{Phalcon_Mvc_Model_1aba208fb458006629695c191000d162|:doc:`Phalcon\\Mvc\\Model\\ManagerInterface <Phalcon_Mvc_Model_ManagerInterface>`}%

%{Phalcon_Mvc_Model_cc64153fcb9f9eef892abdff0655326e}%

%{Phalcon_Mvc_Model_8a50552bb677aebd6eacf2b682a66036|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`|:doc:`Phalcon\\Mvc\\Model\\TransactionInterface <Phalcon_Mvc_Model_TransactionInterface>`}%

%{Phalcon_Mvc_Model_99273e1f203133bc949cde4e2d31c602}%

.. code-block:: php

    <?php

    use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
    use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
    
    try {
    
      $txManager = new TxManager();
    
      $transaction = $txManager->get();
    
      $robot = new Robots();
      $robot->setTransaction($transaction);
      $robot->name = 'WALL·E';
      $robot->created_at = date('Y-m-d');
      if ($robot->save() == false) {
        $transaction->rollback("Can't save robot");
      }
    
      $robotPart = new RobotParts();
      $robotPart->setTransaction($transaction);
      $robotPart->type = 'head';
      if ($robotPart->save() == false) {
        $transaction->rollback("Robot part cannot be saved");
      }
    
      $transaction->commit();
    
    } catch (TxFailed $e) {
      echo 'Failed, reason: ', $e->getMessage();
    }





%{Phalcon_Mvc_Model_e9ef2ca47bba6cd2b8d58c376074f2e8|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_5b5c8fdaa0f959a7230031c9a8af021c}%

%{Phalcon_Mvc_Model_926f986e31ff4993ee80a37f1bdb73fc}%

%{Phalcon_Mvc_Model_dcd1c671d82c9ca4d9507ecb1b9297d1}%

%{Phalcon_Mvc_Model_349f67b973515dda99e78a7ce87f92a5|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_305ef77eae4dd5fda534bd710394f021}%

%{Phalcon_Mvc_Model_305565b73be9be6c91a910b154f20b73}%

%{Phalcon_Mvc_Model_eca5146574824fc8e6d76afeba26a63d}%

%{Phalcon_Mvc_Model_e388410b93813848c99ab5607f48c81c|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_bb876477e44b94495f4355fbb2463a34}%

%{Phalcon_Mvc_Model_336c52540c653c4214684dcbe4bc4b89|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_02e4cb89a648e763b62a55a45d035b92}%

%{Phalcon_Mvc_Model_53b5b36c9e63bedc2895eb70fdf34f68|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_5bd6427d2d13ef03e3ed385043e1c1eb}%

%{Phalcon_Mvc_Model_87113f1be27f3bbbb2651c931b07818a}%

%{Phalcon_Mvc_Model_6656efe00b0b34f95e598175218022fc}%

%{Phalcon_Mvc_Model_950fc56822300d5f82759b73611185a3}%

%{Phalcon_Mvc_Model_f280077c4cf36855c528dcec649aaad7}%

%{Phalcon_Mvc_Model_0da367d875e791de76bd50764115179d|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_3447c50194356794e1fbaf64fc978680}%

%{Phalcon_Mvc_Model_918470f5b1ec6476024a28497f4d02cf}%

%{Phalcon_Mvc_Model_14a88e3f54be484ddf36a127a394da1f}%

%{Phalcon_Mvc_Model_7073142b7495daf0eeac9d80c66297df|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`}%

%{Phalcon_Mvc_Model_cad17e18446e37b402366997cd667fe0}%

%{Phalcon_Mvc_Model_19ed06b20241afc89b3d0d13adb1608e|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`}%

%{Phalcon_Mvc_Model_f6c247697e7edf78c1293af8c43bff1e}%

%{Phalcon_Mvc_Model_84903271ab9cc46e31da0427c6e63ce9|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_ea5fba82a3647da4c8db864465b301c8}%

.. code-block:: php

    <?php

    $robot->assign(array(
      'type' => 'mechanical',
      'name' => 'Astro Boy',
      'year' => 1952
    ));





%{Phalcon_Mvc_Model_224ce277c0fe843d3b9b8ac6bc6b2358|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_d63627436feae92727292128f4d7955f}%

.. code-block:: php

    <?php

    $robot = \Phalcon\Mvc\Model::cloneResultMap(new Robots(), array(
      'type' => 'mechanical',
      'name' => 'Astro Boy',
      'year' => 1952
    ));





%{Phalcon_Mvc_Model_d2394ebbe0d2c289c4d59ea0b411de1a}%

%{Phalcon_Mvc_Model_5fbed31187debab89f0ebab12af93c3a}%

%{Phalcon_Mvc_Model_f295cfcb2322b64a35f048f2804b864e|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_271bb8ec613a8c8c43f97f195011973e}%

.. code-block:: php

    <?php

    $robot = Phalcon\Mvc\Model::cloneResult(new Robots(), array(
      'type' => 'mechanical',
      'name' => 'Astro Boy',
      'year' => 1952
    ));





%{Phalcon_Mvc_Model_9d8030b3d31c9bebbc6e8273f2d21312|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`}%

%{Phalcon_Mvc_Model_ab1c037781388fce804cd04c96a0bd41}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Model_1499c1af63a87b3cd78713aeabe53fc5%}
     $robots = Robots::find();
     echo "There are ", count($robots), "\n";
    
     //{%Phalcon_Mvc_Model_87078387b9e7d1df974b6134db85d304%}
     $robots = Robots::find("type='mechanical'");
     echo "There are ", count($robots), "\n";
    
     //{%Phalcon_Mvc_Model_8565a9d6967f26ce0d27ddb21a21ea4e%}
     $robots = Robots::find(array("type='virtual'", "order" => "name"));
     foreach ($robots as $robot) {
       echo $robot->name, "\n";
     }
    
     //{%Phalcon_Mvc_Model_815bac1c09b347d15b0d2a88e4b3cacd%}
     $robots = Robots::find(array("type='virtual'", "order" => "name", "limit" => 100));
     foreach ($robots as $robot) {
       echo $robot->name, "\n";
     }





%{Phalcon_Mvc_Model_1fd730298936851505a11d48fe63bc44|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_1c5c91736334264f6a703c4c67c49b88}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Model_757b55639cb0428356f22a0f5233a1f4%}
     $robot = Robots::findFirst();
     echo "The robot name is ", $robot->name;
    
     //{%Phalcon_Mvc_Model_fa26e0247c99764efc46dcd460d4ecdd%}
     $robot = Robots::findFirst("type='mechanical'");
     echo "The first mechanical robot name is ", $robot->name;
    
     //{%Phalcon_Mvc_Model_d3f491a4553e16e050d7435bc9820fba%}
     $robot = Robots::findFirst(array("type='virtual'", "order" => "name"));
     echo "The first virtual robot name is ", $robot->name;





%{Phalcon_Mvc_Model_cfe0587d9ae14727699c26604bf9d003|:doc:`Phalcon\\Mvc\\Model\\Criteria <Phalcon_Mvc_Model_Criteria>`|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_b7acfb74a6aa874201e99a7e56f0be23}%

%{Phalcon_Mvc_Model_215fc192bec38f843511eb4128249c76}%

%{Phalcon_Mvc_Model_e90bcee309c4f57b75317b4004dcca2a}%

%{Phalcon_Mvc_Model_44e5a667d88dd37187dbd6089631e06b|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`}%

%{Phalcon_Mvc_Model_76b883f4fce7831891536e18eddce51c}%

%{Phalcon_Mvc_Model_20cef62e45614c5ff8c82f3db97ee449}%

%{Phalcon_Mvc_Model_78c129eb31527e3951051a3eea8a5984}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Model_1499c1af63a87b3cd78713aeabe53fc5%}
     $number = Robots::count();
     echo "There are ", $number, "\n";
    
     //{%Phalcon_Mvc_Model_87078387b9e7d1df974b6134db85d304%}
     $number = Robots::count("type='mechanical'");
     echo "There are ", $number, " mechanical robots\n";





%{Phalcon_Mvc_Model_51d1679bd3b5b4681ad0eaaa426ae2dc}%

%{Phalcon_Mvc_Model_7c2514aa7cc7eb679883fdd77724da8b}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Model_6fdb55776f728d283e1ad3f66cba9132%}
     $sum = Robots::sum(array('column' => 'price'));
     echo "The total price of robots is ", $sum, "\n";
    
     //{%Phalcon_Mvc_Model_dd41e20502b32dbd4bc0957860f4d95c%}
     $sum = Robots::sum(array("type='mechanical'", 'column' => 'price'));
     echo "The total price of mechanical robots is  ", $sum, "\n";





%{Phalcon_Mvc_Model_67781a933014fdbd7ce596de07c1ed7a}%

%{Phalcon_Mvc_Model_849bac96e1110e86bd42be0f07e0c326}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Model_59f5e23ec2ae5b2e4272618f199003c0%}
     $id = Robots::maximum(array('column' => 'id'));
     echo "The maximum robot id is: ", $id, "\n";
    
     //{%Phalcon_Mvc_Model_da03cc550e8d3c31e5aef14d755e3043%}
     $sum = Robots::maximum(array("type='mechanical'", 'column' => 'id'));
     echo "The maximum robot id of mechanical robots is ", $id, "\n";





%{Phalcon_Mvc_Model_99de933be60dfe471bad7dd02e1c9150}%

%{Phalcon_Mvc_Model_4f792bbab73e6cd5bc7a76a7ba98431f}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Model_7574274b4d5eda0c92a41c97057154e5%}
     $id = Robots::minimum(array('column' => 'id'));
     echo "The minimum robot id is: ", $id;
    
     //{%Phalcon_Mvc_Model_220042daf8ce78592ac05d84a6f6fd48%}
     $sum = Robots::minimum(array("type='mechanical'", 'column' => 'id'));
     echo "The minimum robot id of mechanical robots is ", $id;





%{Phalcon_Mvc_Model_751efff96a8db1ae1cf355d36cc18bfd}%

%{Phalcon_Mvc_Model_cd4550f4840d3980e95e299e7c3bea1e}%

.. code-block:: php

    <?php

     //{%Phalcon_Mvc_Model_e0174f52b851bf1dd2cf9abe775f962f%}
     $average = Robots::average(array('column' => 'price'));
     echo "The average price is ", $average, "\n";
    
     //{%Phalcon_Mvc_Model_39cde0a32b036eb4023c7a408fe9bdec%}
     $average = Robots::average(array("type='mechanical'", 'column' => 'price'));
     echo "The average price of mechanical robots is ", $average, "\n";





%{Phalcon_Mvc_Model_f47d0f98b19e11f53daa67eff0ab6490}%

%{Phalcon_Mvc_Model_f99bf1ab50d5a2b74a0dac8fab33b149}%

%{Phalcon_Mvc_Model_efc588f4ffbf7acb46db3cebddac394c}%

%{Phalcon_Mvc_Model_5e15fe75536353a7b09e6220bf5e6b7f}%

%{Phalcon_Mvc_Model_fbf0399ebfc7f8cff11f317323eb35e9}%

%{Phalcon_Mvc_Model_81763188c0e83ea527b222f05affffc8}%

%{Phalcon_Mvc_Model_01e9635d06d021bc1b6bde808a8286b5|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`|:doc:`Phalcon\\Mvc\\Model\\MessageInterface <Phalcon_Mvc_Model_MessageInterface>`}%

%{Phalcon_Mvc_Model_0957aac3d3f7b9ddef640d8851cd3376}%

.. code-block:: php

    <?php

     use \Phalcon\Mvc\Model\Message as Message;
    
     class Robots extends Phalcon\Mvc\Model
     {
    
       public function beforeSave()
       {
         if ($this->name == 'Peter') {
            $message = new Message("Sorry, but a robot cannot be named Peter");
            $this->appendMessage($message);
         }
       }
     }





%{Phalcon_Mvc_Model_79525fee81d0d4d2f804c70c68343021|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`|:doc:`Phalcon\\Mvc\\Model\\ValidatorInterface <Phalcon_Mvc_Model_ValidatorInterface>`}%

%{Phalcon_Mvc_Model_6d51137d07f724c3dcf00e535777fb51}%

.. code-block:: php

    <?php

    use Phalcon\Mvc\Model\Validator\ExclusionIn as ExclusionIn;
    
    class Subscriptors extends Phalcon\Mvc\Model
    {
    
    public function validation()
      {
     		$this->validate(new ExclusionIn(array(
    		'field' => 'status',
    		'domain' => array('A', 'I')
    	)));
    	if ($this->validationHasFailed() == true) {
    		return false;
    	}
    }
    
    }





%{Phalcon_Mvc_Model_5fbc57c37116623f09aefb62a97fdb0a}%

%{Phalcon_Mvc_Model_b62a0696d8ccc76212fbf8e99273f8df}%

.. code-block:: php

    <?php

    use Phalcon\Mvc\Model\Validator\ExclusionIn as ExclusionIn;
    
    class Subscriptors extends Phalcon\Mvc\Model
    {
    
    public function validation()
      {
     		$this->validate(new ExclusionIn(array(
    		'field' => 'status',
    		'domain' => array('A', 'I')
    	)));
    	if ($this->validationHasFailed() == true) {
    		return false;
    	}
    }
    
    }





%{Phalcon_Mvc_Model_331cd8e9a1f4e85f80576996fbec2d18|:doc:`Phalcon\\Mvc\\Model\\MessageInterface <Phalcon_Mvc_Model_MessageInterface>`}%

%{Phalcon_Mvc_Model_5577872af356ca5ceebe7cff99019cae}%

.. code-block:: php

    <?php

    $robot = new Robots();
    $robot->type = 'mechanical';
    $robot->name = 'Astro Boy';
    $robot->year = 1952;
    if ($robot->save() == false) {
      	echo "Umh, We can't store robots right now ";
      	foreach ($robot->getMessages() as $message) {
    		echo $message;
    	}
    } else {
      	echo "Great, a new robot was saved successfully!";
    }





%{Phalcon_Mvc_Model_bb8914149e2c05ee88b027ece5883fcf}%

%{Phalcon_Mvc_Model_92480168f29857f285941efe2f18846f}%

%{Phalcon_Mvc_Model_e53992d57eda1fcd5943f7cfdb4c5ebe}%

%{Phalcon_Mvc_Model_907ac7965e5887bb653fcbfbb9f9c2de}%

%{Phalcon_Mvc_Model_05508ced8fb2a3229ab72e6229c52b1e}%

%{Phalcon_Mvc_Model_17851fea6c7e57406272cc855f8c5f35}%

%{Phalcon_Mvc_Model_a67a23c55246f25f341fbca38d4b4b46}%

%{Phalcon_Mvc_Model_1a2f6b86c007d58f67f55dd8e77fa686}%

%{Phalcon_Mvc_Model_4ff343e39a492da6de2ff25afaf35a97}%

%{Phalcon_Mvc_Model_48473380b374580da9519a846eb8eed6}%

%{Phalcon_Mvc_Model_1c20763180d7e293ce6a283abd1c5cb1}%

%{Phalcon_Mvc_Model_007c4fb5f3aa4db808aebcefc6fe5367}%

%{Phalcon_Mvc_Model_5cd4e5c737c5df570de3537f229f5c67}%

%{Phalcon_Mvc_Model_af84eb598267440d094a636c17d58f94}%

%{Phalcon_Mvc_Model_fb09904d2b8a7e4c52c2eab2c97d71b8}%

%{Phalcon_Mvc_Model_c60ed60ea18062184c0de0e146946c2b}%

%{Phalcon_Mvc_Model_f374262e7ac3f93d3d98de28c2530308}%

%{Phalcon_Mvc_Model_556b2f5c62ce3e9b70a75842a01e306c}%

%{Phalcon_Mvc_Model_02ecd593c4e53339571ddfbbc455a356}%

%{Phalcon_Mvc_Model_72042f6ce5f6e8dafa0c4256f3f631b4}%

.. code-block:: php

    <?php

    //{%Phalcon_Mvc_Model_fd14d1f4d3a573a35b9c4572e88442a3%}
    $robot = new Robots();
    $robot->type = 'mechanical';
    $robot->name = 'Astro Boy';
    $robot->year = 1952;
    $robot->save();
    
    //{%Phalcon_Mvc_Model_d671f574086a5eb77047a14df23d8b7f%}
    $robot = Robots::findFirst("id=100");
    $robot->name = "Biomass";
    $robot->save();





%{Phalcon_Mvc_Model_f3eace751002ac78df7070c87adcec04}%

%{Phalcon_Mvc_Model_cbb9872da1df808b5d15e88c81db95d9}%

.. code-block:: php

    <?php

    //{%Phalcon_Mvc_Model_fd14d1f4d3a573a35b9c4572e88442a3%}
    $robot = new Robots();
    $robot->type = 'mechanical';
    $robot->name = 'Astro Boy';
    $robot->year = 1952;
    $robot->create();
    
      //{%Phalcon_Mvc_Model_fcaa05b663d28c9f9c257a2e1df7cf74%}
      $robot = new Robots();
      $robot->create(array(
          'type' => 'mechanical',
          'name' => 'Astroy Boy',
          'year' => 1952
      ));





%{Phalcon_Mvc_Model_602ecfde251c1d982105fa1f549b2fc3}%

%{Phalcon_Mvc_Model_1a5913211460dc0e3a3f461b5d474e38}%

.. code-block:: php

    <?php

    //{%Phalcon_Mvc_Model_d671f574086a5eb77047a14df23d8b7f%}
    $robot = Robots::findFirst("id=100");
    $robot->name = "Biomass";
    $robot->update();





%{Phalcon_Mvc_Model_6bc264b1ff9d7da0a92aba43b7434fc8}%

%{Phalcon_Mvc_Model_69fa51fa19e89c90ab5a559889604610}%

.. code-block:: php

    <?php

    $robot = Robots::findFirst("id=100");
    $robot->delete();
    
    foreach (Robots::find("type = 'mechanical'") as $robot) {
       $robot->delete();
    }





%{Phalcon_Mvc_Model_b705a5fdc31459dd0604f793d28240e2}%

%{Phalcon_Mvc_Model_00a466a9565b33ee8d53cf519f96cb67}%

%{Phalcon_Mvc_Model_7d4dd61adc30caf9d7e7896c9272579c}%

%{Phalcon_Mvc_Model_fc91a9f199f437cd77d804b9dd6a51cc}%

%{Phalcon_Mvc_Model_e27ed85f6b0813f07ea07b8995f6e013}%

%{Phalcon_Mvc_Model_7b3232a70545725a7ed98a9065a6121d}%

%{Phalcon_Mvc_Model_7b793f1f8fc0c0c603c6f68d21385cde}%

%{Phalcon_Mvc_Model_541ca63c8f0e9cdb167f3c5dff81b126}%

.. code-block:: php

    <?php

     echo $robot->readAttribute('name');





%{Phalcon_Mvc_Model_5edaecf6057a10ae28a47003da1f9669}%

%{Phalcon_Mvc_Model_7c2a8da2a9aeb392f8b2784da24c73a8}%

.. code-block:: php

    <?php

     	$robot->writeAttribute('name', 'Rosey');





%{Phalcon_Mvc_Model_cd72fbe98487cdc805ec7f5131cbc4b1}%

%{Phalcon_Mvc_Model_15e1b9f1457e4cf9e0c7e0c8cb567a7d}%

.. code-block:: php

    <?php

    class Robots extends \Phalcon\Mvc\Model
    {
    
       public function initialize()
       {
           $this->skipAttributes(array('price'));
       }
    
    }





%{Phalcon_Mvc_Model_a6bf90a29f6449bbd4ad5a1b7c0c8ccf}%

%{Phalcon_Mvc_Model_95a79802cedf91dc5ffd93931c8c48b8}%

.. code-block:: php

    <?php

    class Robots extends \Phalcon\Mvc\Model
    {
    
       public function initialize()
       {
           $this->skipAttributesOnCreate(array('created_at'));
       }
    
    }





%{Phalcon_Mvc_Model_f116b08e12b7a22becaf36170020bde3}%

%{Phalcon_Mvc_Model_d13a0252f65a23dd10d23b7cf20058cf}%

.. code-block:: php

    <?php

    class Robots extends \Phalcon\Mvc\Model
    {
    
       public function initialize()
       {
           $this->skipAttributesOnUpdate(array('modified_in'));
       }
    
    }





%{Phalcon_Mvc_Model_5719576a3724aecc6cd3c0770e8e768e|:doc:`Phalcon\\Mvc\\Model\\Relation <Phalcon_Mvc_Model_Relation>`}%

%{Phalcon_Mvc_Model_2b5dea407d811ec0c9e7f11f1a47d4c3}%

.. code-block:: php

    <?php

    class Robots extends \Phalcon\Mvc\Model
    {
    
       public function initialize()
       {
           $this->hasOne('id', 'RobotsDescription', 'robots_id');
       }
    
    }





%{Phalcon_Mvc_Model_fdc62eea60c87adcfdfadfb6645980c3|:doc:`Phalcon\\Mvc\\Model\\Relation <Phalcon_Mvc_Model_Relation>`}%

%{Phalcon_Mvc_Model_1581cae93cf0f62c5ed5171be52257dd}%

.. code-block:: php

    <?php

    class RobotsParts extends \Phalcon\Mvc\Model
    {
    
       public function initialize()
       {
           $this->belongsTo('robots_id', 'Robots', 'id');
       }
    
    }





%{Phalcon_Mvc_Model_6c019cda82cd6a8a7e1b46cf56069a3e|:doc:`Phalcon\\Mvc\\Model\\Relation <Phalcon_Mvc_Model_Relation>`}%

%{Phalcon_Mvc_Model_0b1b431f9631a1c0cf881ae36e22818e}%

.. code-block:: php

    <?php

    class Robots extends \Phalcon\Mvc\Model
    {
    
       public function initialize()
       {
           $this->hasMany('id', 'RobotsParts', 'robots_id');
       }
    
    }





%{Phalcon_Mvc_Model_519e88eb131b7bcaee4fb13a3e5f3c9d|:doc:`Phalcon\\Mvc\\Model\\Relation <Phalcon_Mvc_Model_Relation>`}%

%{Phalcon_Mvc_Model_e255a193bdc01e809fabad2954d9874a}%

.. code-block:: php

    <?php

    class Robots extends \Phalcon\Mvc\Model
    {
    
       public function initialize()
       {
           //{%Phalcon_Mvc_Model_6c940784ee6db944b2f35473558a1cb7%}
           $this->hasManyToMany(
    		'id',
    		'RobotsParts',
    		'robots_id',
    		'parts_id',
    		'Parts',
    		'id'
    	);
       }
    
    }





%{Phalcon_Mvc_Model_b6c262c608f2ea3dc9a78ca6ae11335a|:doc:`Phalcon\\Mvc\\Model\\BehaviorInterface <Phalcon_Mvc_Model_BehaviorInterface>`}%

%{Phalcon_Mvc_Model_b23848f0b9d6f9f90aa4a94a3a88b3aa}%

.. code-block:: php

    <?php

    use Phalcon\Mvc\Model\Behavior\Timestampable;
    
    class Robots extends \Phalcon\Mvc\Model
    {
    
       public function initialize()
       {
    	$this->addBehavior(new Timestampable(array(
    		'onCreate' => array(
    			'field' => 'created_at',
    			'format' => 'Y-m-d'
    		)
    	)));
       }
    
    }





%{Phalcon_Mvc_Model_9d73d5dce34f81a03b6a80890b1ba5d6}%

%{Phalcon_Mvc_Model_4ee1c0e3a6fe4ee26561f41b5560465e}%

.. code-block:: php

    <?php

    class Robots extends \Phalcon\Mvc\Model
    {
    
       public function initialize()
       {
    	$this->keepSnapshots(true);
       }
    
    }





%{Phalcon_Mvc_Model_1f3174cb4a958be754922df34aeab8b9}%

%{Phalcon_Mvc_Model_097d45b0421f0eb78ec3b944718e3e96}%

%{Phalcon_Mvc_Model_2c55561ea43e056aac82e7d2406ad770}%

%{Phalcon_Mvc_Model_e9e34ec71da9e7b5208a3ddbcc43fa12}%

%{Phalcon_Mvc_Model_ab4bc0cdc25eb07fad02e847d47b9b0b}%

%{Phalcon_Mvc_Model_b2df9d90170eee5747d4249527362617}%

%{Phalcon_Mvc_Model_9b53704a197c846c3904cc00e3bb59eb}%

%{Phalcon_Mvc_Model_9728315d3c7b1ab467e5962610fbb17c}%

%{Phalcon_Mvc_Model_a5fdc361367d19283b4cd11ea56d923a}%

%{Phalcon_Mvc_Model_6cd7233659774682fe434fc0d26035df}%

%{Phalcon_Mvc_Model_81b15429931415b14ce9ac6ef8fa54b6}%

%{Phalcon_Mvc_Model_d312553845e3b36b15a10c9e241200a7}%

.. code-block:: php

    <?php

    class Robots extends \Phalcon\Mvc\Model
    {
    
       public function initialize()
       {
    	$this->useDynamicUpdate(true);
       }
    
    }





%{Phalcon_Mvc_Model_3f2dcb2002d3633468c56dee245e7f6b|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`}%

%{Phalcon_Mvc_Model_f580ba63e18a688de4c42b53a8da76bd}%

%{Phalcon_Mvc_Model_d7698d0eebf824265773c4031bfea0b1}%

%{Phalcon_Mvc_Model_fb441b5bace34eb05f87804a7ac5ae5d}%

%{Phalcon_Mvc_Model_de674efab64cbe1546d0cc296de016a8}%

%{Phalcon_Mvc_Model_21f5a0b3028695c4ea443866723bdae2}%

%{Phalcon_Mvc_Model_87e400cf52e7446e9a954102f37e2e2f}%

%{Phalcon_Mvc_Model_7531f727f73e706c7ded48744a7c2a80}%

%{Phalcon_Mvc_Model_f94ede5debadfc56a77b74f980918894}%

%{Phalcon_Mvc_Model_59a871bffabf0a822b09af9438ad83b9}%

%{Phalcon_Mvc_Model_addb4402cee7a9d86d62fbcab065b137|:doc:`Phalcon\\Mvc\\Model\\Resultset <Phalcon_Mvc_Model_Resultset>`}%

%{Phalcon_Mvc_Model_4c5841f25483e8b50e9591281bd559c3}%

%{Phalcon_Mvc_Model_79162e9d4682bd79651d971eb2775d0a}%

%{Phalcon_Mvc_Model_5a9ed324cbc19dff101275d738a5dde9}%

%{Phalcon_Mvc_Model_e4a1eb998f48d9e9518e537dd9bf4090}%

%{Phalcon_Mvc_Model_650e92aafab0a4c3ee8fcc9d8917d814}%

%{Phalcon_Mvc_Model_b998345afa7556b688181bf3ab07c22d}%

%{Phalcon_Mvc_Model_5712a0fc046b2823070d4e91fff6bbbb}%

%{Phalcon_Mvc_Model_74de680a447521feb8ab2c650aa502b9}%

%{Phalcon_Mvc_Model_42222bd9f5ddaf52035030f002e5a7de}%

.. code-block:: php

    <?php

     var_dump($robot->dump());





%{Phalcon_Mvc_Model_6cdfea448abea4dff0c4587d6a1a093c}%

%{Phalcon_Mvc_Model_aee061a540ff24e6f0999f437c33de16}%

.. code-block:: php

    <?php

     print_r($robot->toArray());





%{Phalcon_Mvc_Model_ec82c8b3088594c9db6a3c25f726cbba}%

%{Phalcon_Mvc_Model_9fc77d6b8d6a59154181a751b66f5254}%

