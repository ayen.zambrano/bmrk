%{Phalcon_Logger_Adapter_Stream_5425b906f7cec018d0104c20eb343afb}%
==========================================

%{Phalcon_Logger_Adapter_Stream_10dc7e31f7f341b338fd2727d44d8aa8|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_Stream_06555c5c54842ff80b30f63285905d43|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_Stream_acbcc9194f18fc9c56a990767dbf055b}%

.. code-block:: php

    <?php

    $logger = new \Phalcon\Logger\Adapter\Stream("php://stderr");
    $logger->log("This is a message");
    $logger->log("This is an error", \Phalcon\Logger::ERROR);
    $logger->error("This is another error");




%{Phalcon_Logger_Adapter_Stream_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Logger_Adapter_Stream_549b3fb671e62239011fbe0a37668f4d}%

%{Phalcon_Logger_Adapter_Stream_35884d923b2f9b4112dac15fa2576140}%

%{Phalcon_Logger_Adapter_Stream_267d91900a5d67e4412652f783bc0336|:doc:`Phalcon\\Logger\\Formatter\\Line <Phalcon_Logger_Formatter_Line>`}%

%{Phalcon_Logger_Adapter_Stream_d403d5f385f19f8b13752601556c4e51}%

%{Phalcon_Logger_Adapter_Stream_6d6680aae8523ed3b1f4eb30d9639837}%

%{Phalcon_Logger_Adapter_Stream_8fd3d75b4d71e4e7e96d1d3d0dd852c3}%

%{Phalcon_Logger_Adapter_Stream_1b9666bd6903c29f3a21be0e36db433f}%

%{Phalcon_Logger_Adapter_Stream_b17425a56dfeebdb4fe4f995b044cc99}%

%{Phalcon_Logger_Adapter_Stream_5768b7794ec40ff74e00025a7116a5dd|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_Stream_7179b7a0bdedce8790bec183015cd827}%

%{Phalcon_Logger_Adapter_Stream_5f2d85b87eb2dd3fea10e839482c8fe5}%

%{Phalcon_Logger_Adapter_Stream_b7348a52ab6dd55a4e5fb2b7d3ba889e}%

%{Phalcon_Logger_Adapter_Stream_76b7fb86f583d0ff1250f5240889ce61|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`|:doc:`Phalcon\\Logger\\FormatterInterface <Phalcon_Logger_FormatterInterface>`}%

%{Phalcon_Logger_Adapter_Stream_5b5292c115a695fb23679317ba51b8b1}%

%{Phalcon_Logger_Adapter_Stream_5f0e7ec9f0e24d8ffe897df2b66c52f3|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_Stream_26c40cdf4c3b0d59d1dd64f038e0d9a0}%

%{Phalcon_Logger_Adapter_Stream_4baa6ec59279324d99f5f971a34ecf17|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_Stream_ae86336403bef4c94abd68876f8583ab}%

%{Phalcon_Logger_Adapter_Stream_b29186d9b66beb6ac18431eacf78d698|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_Stream_974c1f35cd09f2112bea68c83c0cb09e}%

%{Phalcon_Logger_Adapter_Stream_4dc3c2827f3027e566b320c6627cb5ec|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_Stream_7658a187f1ddda6c757e1ccbc6b0a3b4}%

%{Phalcon_Logger_Adapter_Stream_6745e19360c8bb5a97b97a7b767fad9a}%

%{Phalcon_Logger_Adapter_Stream_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Logger_Adapter_Stream_72ab7f38361f7569af4acbe3874b2be7|:doc:`Phalcon\\Logger\\Adapter <Phalcon_Logger_Adapter>`}%

%{Phalcon_Logger_Adapter_Stream_f55eae329bf25a51595453ecc14d268a}%

%{Phalcon_Logger_Adapter_Stream_d1e0124a0d450db652392bef382c05a7|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_Stream_41fb655515dd67d271d1668b99ae62c8}%

%{Phalcon_Logger_Adapter_Stream_e3d40307f34f2899f48c72750a4182d7|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_Stream_b547549878361a06f0705a88d13c30d1}%

%{Phalcon_Logger_Adapter_Stream_ed85a4c98fdaff00ebb347302f53cefc|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_Stream_ba89f314766499b7979510665f4b7ac1}%

%{Phalcon_Logger_Adapter_Stream_79da6d2e38ab7c46c7f2c88abbed5d87|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_Stream_1ddbc6e136173b1e10a397223b124fd1}%

%{Phalcon_Logger_Adapter_Stream_0db6be89539f1664c08ab7bfd9ae3b53|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_Stream_44f68c546de28aea3513d007f11708e8}%

%{Phalcon_Logger_Adapter_Stream_c03a07586f08a513f6aecb4dff96aff2|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_Stream_284089fdb19265df523bcdeb006099bf}%

%{Phalcon_Logger_Adapter_Stream_4d779534f5514bd30ec3d489b3b40d09|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_Stream_dc8fa89976b49d9fda57eb5f0ad47406}%

%{Phalcon_Logger_Adapter_Stream_216d286be4c085b14c92cec26b3f6e9d|:doc:`Phalcon\\Logger\\AdapterInterface <Phalcon_Logger_AdapterInterface>`}%

%{Phalcon_Logger_Adapter_Stream_8f730ff32266166faffa41db189b5721}%

