%{Phalcon_Mvc_Model_Validator_Uniqueness_c4e91abc0172bc87c27dc2026474f850}%
====================================================

%{Phalcon_Mvc_Model_Validator_Uniqueness_c598bd7cd0d9e885b8ea2f595c43e7bd|:doc:`Phalcon\\Mvc\\Model\\Validator <Phalcon_Mvc_Model_Validator>`}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_66c90eb4bcaecae7728cb366ede64588|:doc:`Phalcon\\Mvc\\Model\\ValidatorInterface <Phalcon_Mvc_Model_ValidatorInterface>`}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_3951bca09bf815f9b1288601ec38f7e6}%

.. code-block:: php

    <?php

    use Phalcon\Mvc\Model\Validator\Uniqueness as Uniqueness;
    
    class Subscriptors extends Phalcon\Mvc\Model
    {
    
      public function validation()
      {
          $this->validate(new Uniqueness(array(
              'field' => 'email'
          )));
          if ($this->validationHasFailed() == true) {
              return false;
          }
      }
    
    }




%{Phalcon_Mvc_Model_Validator_Uniqueness_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Validator_Uniqueness_4b462e3de6e23ff3eb087ebd5b218937|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_17e36faf7954b362f8bddeac1763cc5c}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_2acb24407cb662827b35e8b7d33a782f}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_cf2d2bc276e4527b2f7a75e910a0ccdd}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_e6bac65b74ab9e71362f7547f9cac8d7}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_7412479f3376d20791a96626bb27abd1}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_205ab0cc247e24d5a27000aff651275e}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_2054770bfeb7ee2bceb6343afc43d355}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_c2ef55f3af7f83c76019916dd43ac15d}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_9b5b1f1b759bb0172acbd5b115e6ae93}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_3b76c64bdf82a7b2b9e4ca31e72200cb}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_206991b1bd20a31a578db9ead9dbf574}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_aa5d85e2efa37920a41e2865e9b154be}%

%{Phalcon_Mvc_Model_Validator_Uniqueness_90db846e091e2484702e7fd2225f9d67}%

