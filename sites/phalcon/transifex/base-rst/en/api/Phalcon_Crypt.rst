%{Phalcon_Crypt_69636217fda64cd5c8c14ec0aa96a14c}%
========================

%{Phalcon_Crypt_29a20ac680f543b55cab20dc2c6c9d95|:doc:`Phalcon\\CryptInterface <Phalcon_CryptInterface>`}%

%{Phalcon_Crypt_139a0d0ca940b8f553e6ebbcdd12f492}%

.. code-block:: php

    <?php

    $crypt = new Phalcon\Crypt();
    
    $key = 'le password';
    $text = 'This is a secret text';
    
    $encrypted = $crypt->encrypt($text, $key);
    
    echo $crypt->decrypt($encrypted, $key);




%{Phalcon_Crypt_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Crypt_f7bcb32d6b06cac1e12d51fb7ac6a0c5}%

%{Phalcon_Crypt_25ec4b5aaf36b9074150a21c2cc179b7}%

%{Phalcon_Crypt_d839a798b567f1fcc83f662b8108024c}%

%{Phalcon_Crypt_3270f4fe6914c55f79d90312705f7ec7}%

%{Phalcon_Crypt_6313d8c1b6088a854a4a3dc1ee898101}%

%{Phalcon_Crypt_996f5b8c448066378ecbddadbadf7758}%

%{Phalcon_Crypt_35eb30527182d4e9caf5cfc42dbd79c9}%

%{Phalcon_Crypt_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Crypt_200f3759e22a50aee9794f491dc70a29}%

%{Phalcon_Crypt_1c5dbe7974e541847a763cef99186190}%

%{Phalcon_Crypt_1715aea33bd3b55b1fd3c8531f7757e9}%

%{Phalcon_Crypt_206a55a3b4024ed442c31961cb659ba0}%

%{Phalcon_Crypt_701dfc2c786719c5d4ad2d24a73622b7}%

%{Phalcon_Crypt_31386a02b938196ab74285d73f848b0e}%

%{Phalcon_Crypt_d04a8bb0ae9afb18dd9f2a82f5d3a36c}%

%{Phalcon_Crypt_c97e0d62e6f9299015a894b1811c3908}%

%{Phalcon_Crypt_2045e6a3bf5f53ee1608c2e46736c743}%

%{Phalcon_Crypt_fa0b0ad5d528bb6a6cdf821f075b819d}%

%{Phalcon_Crypt_fec438ce12a8a7fd10c6da124b49aaeb}%

%{Phalcon_Crypt_0328bc5ae7e4a8d02e329f64448b5411}%

%{Phalcon_Crypt_ffe5fc4afab26dbcf121357c9b571d9d|:doc:`Phalcon\\CryptInterface <Phalcon_CryptInterface>`}%

%{Phalcon_Crypt_52910719dcc9378861b3398dae7b5376}%

%{Phalcon_Crypt_561faca97086a30d96e463c59c49f26d}%

%{Phalcon_Crypt_82e0534338e9e79e2f9f348dd1cfd760}%

%{Phalcon_Crypt_294e997563530c6cc7f2dca42b40e90b}%

.. code-block:: php

    <?php

    $encrypted = $crypt->encrypt("Ultra-secret text", "encrypt password");





%{Phalcon_Crypt_30c00c7a9e0ab1812e6d66ae6c1e1170}%

%{Phalcon_Crypt_54f81f09d9eda241fbe25c3ba094a7ed}%

.. code-block:: php

    <?php

    echo $crypt->decrypt($encrypted, "decrypt password");





%{Phalcon_Crypt_dd81dff19459ea6d7881815cc9539717}%

%{Phalcon_Crypt_8aecf459e56b0caff02b648b2397c41d}%

%{Phalcon_Crypt_b6ed3da7668088f969ae9aa1098a91fb}%

%{Phalcon_Crypt_cc0ce1c045f3cb0f4f987d29f890e2dc}%

%{Phalcon_Crypt_067a4b239e9bbbf0fee78464af3832e7}%

%{Phalcon_Crypt_68b5a353b3c22abe921ef8fab54806bd}%

%{Phalcon_Crypt_40cb05ca025f16e2b9c988686656c970}%

%{Phalcon_Crypt_c2b56b1bf492600b869840341b2d9282}%

