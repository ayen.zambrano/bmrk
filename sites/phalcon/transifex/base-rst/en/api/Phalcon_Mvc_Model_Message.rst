%{Phalcon_Mvc_Model_Message_98170b4598e29d27a6cc81d400ab21ff}%
======================================

%{Phalcon_Mvc_Model_Message_64442c323e682c49c72975bdb08fddfb|:doc:`Phalcon\\Mvc\\Model\\MessageInterface <Phalcon_Mvc_Model_MessageInterface>`}%

%{Phalcon_Mvc_Model_Message_eb46b1746ffbd36315b800930eb6a330}%

.. code-block:: php

    <?php

    use Phalcon\Mvc\Model\Message as Message;
    
      class Robots extends Phalcon\Mvc\Model
      {
    
        public function beforeSave()
        {
          if ($this->name == 'Peter') {
            $text = "A robot cannot be named Peter";
            $field = "name";
            $type = "InvalidValue";
            $code = 103;
            $message = new Message($text, $field, $type, $code);
            $this->appendMessage($message);
         }
       }
    
     }




%{Phalcon_Mvc_Model_Message_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Message_3dc1b778ee58d3d688772bb29d29d7d5}%

%{Phalcon_Mvc_Model_Message_7a1952e55e2dff578fb82508c1c9e2c0}%

%{Phalcon_Mvc_Model_Message_c68003c722aa3d3ea2e787362fd7b837|:doc:`Phalcon\\Mvc\\Model\\Message <Phalcon_Mvc_Model_Message>`}%

%{Phalcon_Mvc_Model_Message_93da1345fddf41594cc178a2b5c1f09e}%

%{Phalcon_Mvc_Model_Message_9b6af70c1c9980c497b487b7e2dbbccd}%

%{Phalcon_Mvc_Model_Message_ba2e2792170b18e40233d2a50b4ffa17}%

%{Phalcon_Mvc_Model_Message_703c672f5eeb2eda8af8090a539030a4|:doc:`Phalcon\\Mvc\\Model\\Message <Phalcon_Mvc_Model_Message>`}%

%{Phalcon_Mvc_Model_Message_5f0ef0a25de8004b5579d1453608b2de}%

%{Phalcon_Mvc_Model_Message_a7be37c6abda68570ce934f7abce3aaf}%

%{Phalcon_Mvc_Model_Message_e8c4469f274c4d142d9151ed63fb80eb}%

%{Phalcon_Mvc_Model_Message_e6fdc330db10b21ebbc2016bbd11b712|:doc:`Phalcon\\Mvc\\Model\\Message <Phalcon_Mvc_Model_Message>`}%

%{Phalcon_Mvc_Model_Message_bd0d65cb2ebc749357873ddd020251c6}%

%{Phalcon_Mvc_Model_Message_ba49ad370c587abc7e6b8ec74254129c}%

%{Phalcon_Mvc_Model_Message_cd3a4b5cec242bb914c518e21b2f6e27}%

%{Phalcon_Mvc_Model_Message_ed402f65df237dfd916279221c0f12b7|:doc:`Phalcon\\Mvc\\Model\\Message <Phalcon_Mvc_Model_Message>`}%

%{Phalcon_Mvc_Model_Message_2f867102e012fe4453f5615368ed0bea}%

%{Phalcon_Mvc_Model_Message_6c84fcbcf4a9d57fe48dcb3b47e398cc}%

%{Phalcon_Mvc_Model_Message_f5a667f9ad5da7d713474f1f55140e7a}%

%{Phalcon_Mvc_Model_Message_426d8784fb26fd5dbe7e908b210a557a|:doc:`Phalcon\\Mvc\\Model\\Message <Phalcon_Mvc_Model_Message>`|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Message_263c0779c8e1d69f81e78c0cc0109a7a}%

%{Phalcon_Mvc_Model_Message_8d796dbf856a5f0c87b2875a175170c5|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Message_d850a5d2dc77ea6a26be13916bbbffc9}%

%{Phalcon_Mvc_Model_Message_63da8926a73c1d4296b675bb1be32b85}%

%{Phalcon_Mvc_Model_Message_d65a11d2f0cd1ae4b1796516809d1465}%

%{Phalcon_Mvc_Model_Message_42077b1688faf057ec35cd8e5a6dd57f|:doc:`Phalcon\\Mvc\\Model\\Message <Phalcon_Mvc_Model_Message>`}%

%{Phalcon_Mvc_Model_Message_943703b5c20e7d451d80b743cc1cc67b}%

