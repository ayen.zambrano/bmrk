%{Phalcon_Db_Adapter_Pdo_Mysql_9bda8d47af481fac1b8e95e6c76b5578}%
==========================================

%{Phalcon_Db_Adapter_Pdo_Mysql_2540fd1e60509fe46bd72f58a0dbd040|:doc:`Phalcon\\Db\\Adapter\\Pdo <Phalcon_Db_Adapter_Pdo>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_7d0733e1381363bac9600138af495f82|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_4e1510436a397f971ec35a3f0c376447}%

.. code-block:: php

    <?php

    $config = array(
    	"host" => "192.168.0.11",
    	"dbname" => "blog",
    	"port" => 3306,
    	"username" => "sigma",
    	"password" => "secret"
    );
    
    $connection = new Phalcon\Db\Adapter\Pdo\Mysql($config);




%{Phalcon_Db_Adapter_Pdo_Mysql_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Db_Adapter_Pdo_Mysql_8ec673b4673150c9ca219bb45b791cd1}%

%{Phalcon_Db_Adapter_Pdo_Mysql_84c64462ca45fa0db805774782541586}%

%{Phalcon_Db_Adapter_Pdo_Mysql_65f87d688a454981928166290560d7a7|:doc:`Phalcon\\Db\\Column <Phalcon_Db_Column>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_bff1f2169d445b2ee3fa49d40f087e87}%

.. code-block:: php

    <?php

     print_r($connection->describeColumns("posts")); ?>





%{Phalcon_Db_Adapter_Pdo_Mysql_83f173cfdc7e5d123718d3f6444c12d5}%

%{Phalcon_Db_Adapter_Pdo_Mysql_57b1671a9839b169578e92d7c316ab73}%

%{Phalcon_Db_Adapter_Pdo_Mysql_b5ffc3f5622599db5d52762752651671}%

%{Phalcon_Db_Adapter_Pdo_Mysql_ed37bed1251de9cff7cdbbf3a3760924}%

.. code-block:: php

    <?php

     //{%Phalcon_Db_Adapter_Pdo_Mysql_2aa06f72701e946a2ab563fbd6f41364%}
     $connection = new Phalcon\Db\Adapter\Pdo\Mysql(array(
      'host' => '192.168.0.11',
      'username' => 'sigma',
      'password' => 'secret',
      'dbname' => 'blog',
     ));
    
     //{%Phalcon_Db_Adapter_Pdo_Mysql_880b8461cba2e295e1828be8d6eedb0a%}
     $connection->connect();





%{Phalcon_Db_Adapter_Pdo_Mysql_6abcb689d73a6d865888ee9a1de5b318}%

%{Phalcon_Db_Adapter_Pdo_Mysql_8f5e6e8de6f8e7b746125449ad3bb873}%

.. code-block:: php

    <?php

     $statement = $connection->prepare('SELECT * FROM robots WHERE name = :name');
     $pdoResult = $connection->executePrepared($statement, array('name' => 'Voltron'));





%{Phalcon_Db_Adapter_Pdo_Mysql_ad693894faffd60d0feee08418d8770d}%

%{Phalcon_Db_Adapter_Pdo_Mysql_de778171f411ced7fe22f69c9930c97d}%

.. code-block:: php

    <?php

     $statement = $connection->prepare('SELECT * FROM robots WHERE name = :name');
     $pdoResult = $connection->executePrepared($statement, array('name' => 'Voltron'));





%{Phalcon_Db_Adapter_Pdo_Mysql_fcb57104c81c2c4961e4594c582e7c73|:doc:`Phalcon\\Db\\ResultInterface <Phalcon_Db_ResultInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_12b9c6a9c5893b1b1f662bc690326923}%

.. code-block:: php

    <?php

    //{%Phalcon_Db_Adapter_Pdo_Mysql_7c9ca50a0502b5634e20a005ccaa3f46%}
    $resultset = $connection->query("SELECT * FROM robots WHERE type='mechanical'");
    $resultset = $connection->query("SELECT * FROM robots WHERE type=?", array("mechanical"));





%{Phalcon_Db_Adapter_Pdo_Mysql_db25df90800b3dbbdf6a11c13c3b57e7}%

%{Phalcon_Db_Adapter_Pdo_Mysql_a97b1097deab55ea0befc3ef7d8e47c8}%

.. code-block:: php

    <?php

    //{%Phalcon_Db_Adapter_Pdo_Mysql_fc5db8402393ff6b981d3bbfc3eb012f%}
    $success = $connection->execute("INSERT INTO robots VALUES (1, 'Astro Boy')");
    $success = $connection->execute("INSERT INTO robots VALUES (?, ?)", array(1, 'Astro Boy'));





%{Phalcon_Db_Adapter_Pdo_Mysql_59500fec1fc637cf024a21cbd823e029}%

%{Phalcon_Db_Adapter_Pdo_Mysql_ad64bcfa6ba7e9a6cedaaa03d1aaea4c}%

.. code-block:: php

    <?php

    $connection->execute("DELETE FROM robots");
    echo $connection->affectedRows(), ' were deleted';





%{Phalcon_Db_Adapter_Pdo_Mysql_818787ce661ff4a3c485701f63f70a3e}%

%{Phalcon_Db_Adapter_Pdo_Mysql_bf2c77ef5035bc50bcb26da203ab1949}%

%{Phalcon_Db_Adapter_Pdo_Mysql_29a899145e0860c3fe246837cccbcff9}%

%{Phalcon_Db_Adapter_Pdo_Mysql_3adb220a1a388877c0cafba47d034663}%

.. code-block:: php

    <?php

    $escapedStr = $connection->escapeString('some dangerous value');





%{Phalcon_Db_Adapter_Pdo_Mysql_7de9869cfd7976c99fd73912c829e715}%

%{Phalcon_Db_Adapter_Pdo_Mysql_b2034b7bb2aea42468a06f7614f9f190}%

.. code-block:: php

    <?php

     print_r($connection->convertBoundParams('SELECT * FROM robots WHERE name = :name:', array('Bender')));





%{Phalcon_Db_Adapter_Pdo_Mysql_d4d8e77735020b4925bf0e3e06b8bdbe}%

%{Phalcon_Db_Adapter_Pdo_Mysql_61b863e00ed39aaf2250524abb7e0017}%

.. code-block:: php

    <?php

     //{%Phalcon_Db_Adapter_Pdo_Mysql_a6d75204a8bf217cb392074b3ca4cc87%}
     $success = $connection->insert(
         "robots",
         array("Astro Boy", 1952),
         array("name", "year")
     );
    
     //{%Phalcon_Db_Adapter_Pdo_Mysql_10b629cacf9dcb09606f0f50b25a0161%}
     $id = $connection->lastInsertId();





%{Phalcon_Db_Adapter_Pdo_Mysql_e236ee3c360aa5b1aebe801a5d07a3d2}%

%{Phalcon_Db_Adapter_Pdo_Mysql_fe5dce48c5b13658e4f75935b6eb2fa3}%

%{Phalcon_Db_Adapter_Pdo_Mysql_a6d16accc59d42d47e5408a27af01ccc}%

%{Phalcon_Db_Adapter_Pdo_Mysql_10cd19f3c7f746aaccef62a5110cb83e}%

%{Phalcon_Db_Adapter_Pdo_Mysql_924d7ba4c9998b12fb80ed1a3d6fd877}%

%{Phalcon_Db_Adapter_Pdo_Mysql_ca13799e68b46cb24770c655cb161130}%

%{Phalcon_Db_Adapter_Pdo_Mysql_d119073402be42a68c6b2416f9b9fd0c}%

%{Phalcon_Db_Adapter_Pdo_Mysql_cb4dc3e1a61fb0878c95745862a36014}%

%{Phalcon_Db_Adapter_Pdo_Mysql_4d2cd7b10a69c9de5a57529014c2b387}%

%{Phalcon_Db_Adapter_Pdo_Mysql_0c75384a1bd9a12b69ab83406e6435d8}%

.. code-block:: php

    <?php

    $connection->begin();
    var_dump($connection->isUnderTransaction()); //{%Phalcon_Db_Adapter_Pdo_Mysql_b326b5062b2f0e69046810717534cb09%}





%{Phalcon_Db_Adapter_Pdo_Mysql_754afc3c3c01f957808674597b5818eb}%

%{Phalcon_Db_Adapter_Pdo_Mysql_98e9136cd119cd76e168d9a89697a175}%

%{Phalcon_Db_Adapter_Pdo_Mysql_6d158497b7e6a27c92f81ed571655536|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_994924f85ea614b243b7b4d954e37a48}%

%{Phalcon_Db_Adapter_Pdo_Mysql_4317ddf942f0506bbe514503f86f2518|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_Db_Adapter_Pdo_Mysql_104c3f1511a5cf5c6badef85697926e6}%

%{Phalcon_Db_Adapter_Pdo_Mysql_7405adc04f8e073fea4449c9ec0dcdbb}%

%{Phalcon_Db_Adapter_Pdo_Mysql_123f05f953de683899f381c0dde1208b|:doc:`Phalcon\\Db\\DialectInterface <Phalcon_Db_DialectInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_dfbf23645602677fc82754d84973dd18}%

%{Phalcon_Db_Adapter_Pdo_Mysql_7a1a13f667f108ba4d785f4815641f1c}%

%{Phalcon_Db_Adapter_Pdo_Mysql_5fb298f91e097316bdd41a45592bf05d}%

.. code-block:: php

    <?php

    //{%Phalcon_Db_Adapter_Pdo_Mysql_b11742e0634a5ed06167a6a27fd6bdf6%}
    $robot = $connection->fetchOne("SELECT * FROM robots");
    print_r($robot);
    
    //{%Phalcon_Db_Adapter_Pdo_Mysql_76e1f206704b79f65bb244b53a2d14ea%}
    $robot = $connection->fetchOne("SELECT * FROM robots", Phalcon\Db::FETCH_ASSOC);
    print_r($robot);





%{Phalcon_Db_Adapter_Pdo_Mysql_c01991696855a600f5bb2a45d2948eb4}%

%{Phalcon_Db_Adapter_Pdo_Mysql_533879dcfc578e9d9beccfa4e36332c6}%

.. code-block:: php

    <?php

    //{%Phalcon_Db_Adapter_Pdo_Mysql_8ae30418bcb37bf7e95ce3ac42cae77c%}
    $robots = $connection->fetchAll("SELECT * FROM robots", Phalcon\Db::FETCH_ASSOC);
    foreach ($robots as $robot) {
    	print_r($robot);
    }
    
      //{%Phalcon_Db_Adapter_Pdo_Mysql_3d0f42eaf81337476a3aa5ecc8e536e9%}
      $robots = $connection->fetchAll("SELECT * FROM robots WHERE name LIKE :name",
    	Phalcon\Db::FETCH_ASSOC,
    	array('name' => '%robot%')
      );
    foreach($robots as $robot){
    	print_r($robot);
    }





%{Phalcon_Db_Adapter_Pdo_Mysql_bbd5c60258c2f22b9972191c1b554413}%

%{Phalcon_Db_Adapter_Pdo_Mysql_d9fdc1d16462ce210a1f40f64f0db506}%

.. code-block:: php

    <?php

     //{%Phalcon_Db_Adapter_Pdo_Mysql_a6d75204a8bf217cb392074b3ca4cc87%}
     $success = $connection->insert(
         "robots",
         array("Astro Boy", 1952),
         array("name", "year")
     );
    
     //{%Phalcon_Db_Adapter_Pdo_Mysql_f7bb4646596b31ceed34225ee4851f2b%}
     INSERT INTO `robots` (`name`, `year`) VALUES ("Astro boy", 1952);





%{Phalcon_Db_Adapter_Pdo_Mysql_89c8eb3b3d34bde72a34e2fe5d6b85a0}%

%{Phalcon_Db_Adapter_Pdo_Mysql_a358e27bdde98bbf702631fdb73783d5}%

.. code-block:: php

    <?php

     //{%Phalcon_Db_Adapter_Pdo_Mysql_86f7dc0b6663836e4c275c21174bad74%}
     $success = $connection->update(
         "robots",
         array("name"),
         array("New Astro Boy"),
         "id = 101"
     );
    
     //{%Phalcon_Db_Adapter_Pdo_Mysql_f7bb4646596b31ceed34225ee4851f2b%}
     UPDATE `robots` SET `name` = "Astro boy" WHERE id = 101





%{Phalcon_Db_Adapter_Pdo_Mysql_a864d1fad486e99b944cd6be24eee653}%

%{Phalcon_Db_Adapter_Pdo_Mysql_26df2fdb3c3a0c35d6a2b2a9ce6cdd6d}%

.. code-block:: php

    <?php

     //{%Phalcon_Db_Adapter_Pdo_Mysql_b8901a82eceebe8d7e34dd317f881fd1%}
     $success = $connection->delete(
         "robots",
         "id = 101"
     );
    
     //{%Phalcon_Db_Adapter_Pdo_Mysql_7992087a502ec3273b0b5e847292ea81%}
     DELETE FROM `robots` WHERE `id` = 101





%{Phalcon_Db_Adapter_Pdo_Mysql_b545b0a41d6f81acbb45de05da3b80f8}%

%{Phalcon_Db_Adapter_Pdo_Mysql_cef775cddd996a0683f57c5242219e20}%

%{Phalcon_Db_Adapter_Pdo_Mysql_6c1b1b349e3688631a9f4a4043efe3dd}%

%{Phalcon_Db_Adapter_Pdo_Mysql_5a4a993ea6e204ee5f9f2fa04da81a1f}%

.. code-block:: php

    <?php

     	echo $connection->limit("SELECT * FROM robots", 5);





%{Phalcon_Db_Adapter_Pdo_Mysql_dac2a145d75d162989e1632fe728c8c3}%

%{Phalcon_Db_Adapter_Pdo_Mysql_733ee4acdd7aca845798bc20c0cb008f}%

.. code-block:: php

    <?php

     	var_dump($connection->tableExists("blog", "posts"));





%{Phalcon_Db_Adapter_Pdo_Mysql_53293abb38ef4262b8a151f972c134c2}%

%{Phalcon_Db_Adapter_Pdo_Mysql_b471ca007fa5635663442ca313939f1f}%

.. code-block:: php

    <?php

     var_dump($connection->viewExists("active_users", "posts"));





%{Phalcon_Db_Adapter_Pdo_Mysql_f41446246117766839f41b597e9d8af7}%

%{Phalcon_Db_Adapter_Pdo_Mysql_52d3aac013e30a8356f7bf56e85433d2}%

%{Phalcon_Db_Adapter_Pdo_Mysql_d393ca0c20ead8e0df2283c9e3dac2b8}%

%{Phalcon_Db_Adapter_Pdo_Mysql_2d4abc0c85ba918abd9ea6c24091abfc}%

%{Phalcon_Db_Adapter_Pdo_Mysql_132adcfb01b0a38a7d74ded1ced72ca8}%

%{Phalcon_Db_Adapter_Pdo_Mysql_a7fc71aafb1ecd0ff268d7c38c662c06}%

%{Phalcon_Db_Adapter_Pdo_Mysql_0738937e455813171cd30c7ca8a042a1}%

%{Phalcon_Db_Adapter_Pdo_Mysql_d22fc1207292217bf710c07457192c75}%

%{Phalcon_Db_Adapter_Pdo_Mysql_dfbbb3d607b01c61ca67d7f14f0fba2d}%

%{Phalcon_Db_Adapter_Pdo_Mysql_000156e030a02184d59d8da1371f3849}%

%{Phalcon_Db_Adapter_Pdo_Mysql_f92fb89296514e137f7dd30eb69423d1}%

%{Phalcon_Db_Adapter_Pdo_Mysql_a804956605ee1dddfa70dc8a56187382}%

%{Phalcon_Db_Adapter_Pdo_Mysql_28914fdf4f41bd64dea25ee8fb82ed37|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_f42b09293fb9b3b4ef977cfa3b19da02}%

%{Phalcon_Db_Adapter_Pdo_Mysql_8f21639643e4ef2b52bde19006df16f2|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_658ec0a8201eb7d0360751eea7126854}%

%{Phalcon_Db_Adapter_Pdo_Mysql_862a56e7b844f41a225cb8b6e9e3bae5}%

%{Phalcon_Db_Adapter_Pdo_Mysql_7a3c7e655fe7f34e1803d59d587e7d55}%

%{Phalcon_Db_Adapter_Pdo_Mysql_469fcd1386d23fd28e714eb0384061e6|:doc:`Phalcon\\Db\\IndexInterface <Phalcon_Db_IndexInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_206b49008e22eeb21c78be67c9703395}%

%{Phalcon_Db_Adapter_Pdo_Mysql_6cd42200260e39fb607f2e431f4d4f20}%

%{Phalcon_Db_Adapter_Pdo_Mysql_9f5ae026beab44ce8e3068331ef0a022}%

%{Phalcon_Db_Adapter_Pdo_Mysql_d537b4c3baea08d5c9be6d0d32d270ee|:doc:`Phalcon\\Db\\IndexInterface <Phalcon_Db_IndexInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_f5ff85edfe2b24efde0ce1938797471a}%

%{Phalcon_Db_Adapter_Pdo_Mysql_1fb1a813fd01c2656aa45453644a179f}%

%{Phalcon_Db_Adapter_Pdo_Mysql_f52cd75354fb5d64e978ee59cdd0dfe9}%

%{Phalcon_Db_Adapter_Pdo_Mysql_09cff6820d8e73f7f56b5584d7c151e6|:doc:`Phalcon\\Db\\ReferenceInterface <Phalcon_Db_ReferenceInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_7091106149e8afa191924640a7c537f5}%

%{Phalcon_Db_Adapter_Pdo_Mysql_18bd584974db921bb03eb82a0399d5cb}%

%{Phalcon_Db_Adapter_Pdo_Mysql_0f0c2b54afb93884d0bd5c1e081ff67f}%

%{Phalcon_Db_Adapter_Pdo_Mysql_765bbcd00d1cc8184819be58a3e0e0d4|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_55e20593fbd175ee67322cc1187c8ade}%

%{Phalcon_Db_Adapter_Pdo_Mysql_c3ac66330e6b4befa6e6c1445525d9c6}%

%{Phalcon_Db_Adapter_Pdo_Mysql_1f97bb21531bf7a74abf40f789c10861}%

.. code-block:: php

    <?php

     	print_r($connection->listTables("blog"));





%{Phalcon_Db_Adapter_Pdo_Mysql_61d9ed8cf0cbf7198cf5b1897a7aae83}%

%{Phalcon_Db_Adapter_Pdo_Mysql_f23a83679c69fb3e27ee6904d11d4bc5}%

.. code-block:: php

    <?php

    print_r($connection->listViews("blog")); ?>





%{Phalcon_Db_Adapter_Pdo_Mysql_5344ac26ac4d0e0be51cade3e0fe7bf4|:doc:`Phalcon\\Db\\Index <Phalcon_Db_Index>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_dded571f895472a2faafd35a6988b25b}%

.. code-block:: php

    <?php

    print_r($connection->describeIndexes('robots_parts'));





%{Phalcon_Db_Adapter_Pdo_Mysql_a52b422eaba46cd2f2bfb06df94ca573|:doc:`Phalcon\\Db\\Reference <Phalcon_Db_Reference>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_996d3196cca64adc57b2236c50c7df36}%

.. code-block:: php

    <?php

     print_r($connection->describeReferences('robots_parts'));





%{Phalcon_Db_Adapter_Pdo_Mysql_ed07413aabc50a287694c7eb60e246e3}%

%{Phalcon_Db_Adapter_Pdo_Mysql_f427319993001e5061cbca9bf42bae34}%

.. code-block:: php

    <?php

     print_r($connection->tableOptions('robots'));





%{Phalcon_Db_Adapter_Pdo_Mysql_313515397cefd5bee01e27ec7862016d}%

%{Phalcon_Db_Adapter_Pdo_Mysql_4240f3e368588a9a9ed238bf19a11863}%

%{Phalcon_Db_Adapter_Pdo_Mysql_7cd563f23c34710d851e9292bab10c43}%

%{Phalcon_Db_Adapter_Pdo_Mysql_a382c706189656fce42f2a9cd80784e5}%

%{Phalcon_Db_Adapter_Pdo_Mysql_90e554650e267a075b2cc2def9a959f1}%

%{Phalcon_Db_Adapter_Pdo_Mysql_8ea57471de99c4626beb0068a3a0cc82}%

%{Phalcon_Db_Adapter_Pdo_Mysql_79b5a052f7fe6860775bfbabd8f7225b|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_65f84254266985664e7b0041fcd54b9a}%

%{Phalcon_Db_Adapter_Pdo_Mysql_6ce86eec6aa2d966a79318cdf70c922d}%

%{Phalcon_Db_Adapter_Pdo_Mysql_936a103d73aac2ac6a8061e5ce579c62}%

%{Phalcon_Db_Adapter_Pdo_Mysql_fb30e0cc9f54cf9cc03d1574cec29c35}%

%{Phalcon_Db_Adapter_Pdo_Mysql_b68c46d2d0f254d7846ede4084807435}%

%{Phalcon_Db_Adapter_Pdo_Mysql_73f3627d0078c56664dce4c49c24a0a0|:doc:`Phalcon\\Db\\RawValue <Phalcon_Db_RawValue>`}%

%{Phalcon_Db_Adapter_Pdo_Mysql_44713ee7c895fe92dedce30931309159}%

.. code-block:: php

    <?php

     //{%Phalcon_Db_Adapter_Pdo_Mysql_7465da78909b003fe9c9634390518500%}
     $success = $connection->insert(
         "robots",
         array($connection->getDefaultIdValue(), "Astro Boy", 1952),
         array("id", "name", "year")
     );





%{Phalcon_Db_Adapter_Pdo_Mysql_2963d7a8499d2d44d92e86c14829b79e}%

%{Phalcon_Db_Adapter_Pdo_Mysql_74864962afc9a0a54b670ed3f97bd879}%

%{Phalcon_Db_Adapter_Pdo_Mysql_009860fd898917d6d1042d93981a101c}%

%{Phalcon_Db_Adapter_Pdo_Mysql_bf6837aa9e7072ebf3ab752d8a0d0a06}%

%{Phalcon_Db_Adapter_Pdo_Mysql_487f549c089b1ff65ebfabefa56ce6a3}%

%{Phalcon_Db_Adapter_Pdo_Mysql_c3985835d1e4654795856a386b108e02}%

%{Phalcon_Db_Adapter_Pdo_Mysql_ca95dbce98d033dfdb9278e79456b3c3}%

%{Phalcon_Db_Adapter_Pdo_Mysql_a6e492ae4d5dc538bf27544d2adaf1c5}%

%{Phalcon_Db_Adapter_Pdo_Mysql_1742ad4097cf30a1736e45aac08657f0}%

%{Phalcon_Db_Adapter_Pdo_Mysql_57c1971d37fe7408651d816f3304544f}%

%{Phalcon_Db_Adapter_Pdo_Mysql_77776378e1a2445a60c304e8a6c2341c}%

%{Phalcon_Db_Adapter_Pdo_Mysql_6cc3615bd0f815d37c62512c3de3aa59}%

%{Phalcon_Db_Adapter_Pdo_Mysql_96d9a7f50eb6ddd9f627ff9b95dda4f7}%

%{Phalcon_Db_Adapter_Pdo_Mysql_57c1971d37fe7408651d816f3304544f}%

%{Phalcon_Db_Adapter_Pdo_Mysql_2418163d20bf5d2117eb3b861888d640}%

%{Phalcon_Db_Adapter_Pdo_Mysql_57c1971d37fe7408651d816f3304544f}%

%{Phalcon_Db_Adapter_Pdo_Mysql_9ce271ca28fbaed0deb632f7f42adfad}%

%{Phalcon_Db_Adapter_Pdo_Mysql_ac7d79c5075d89af44c82d00bbf623f7}%

%{Phalcon_Db_Adapter_Pdo_Mysql_50c19214ed5be518dec9f68e5bcdf08c}%

%{Phalcon_Db_Adapter_Pdo_Mysql_2b0bc4b05d6684386da7ee7849b1306e}%

