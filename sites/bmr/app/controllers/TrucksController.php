<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class TrucksController extends Controller
{
	public function initialize(){
		$this->view->setTemplateAfter('template');
	}

	public function addtruckAction()
	{
		$this->view->setVar('header_title', "Add New Truck");
		///LIST COMPANY
		$table_company = Company::find();
        foreach ($table_company as $m) {
            $data[] = array(
            	'id' => $m->id ,
                'campname' => $m->campname ,
            );
        }
        $data =json_encode($data);
        $this->view->list_comp =json_decode($data) ;
        ///END LIST COMPANY


        //EMPTY VARIABLES}
        $this->view->company = "" ;
        $this->view->plateno = "" ;
        $this->view->length = "" ;
        $this->view->width = "" ;
        $this->view->heigth = "" ;

        if ($this->request->isPost('save') == true) {
		 	//VARIABLE
		 	$company= $this->request->getPost('company');//POST COMPANY
		 	$this->view->company = $company ;//DISPLAY VALUE COMPANY********************
		 	$plateno= $this->request->getPost('plateno') ;//POST PLATE NO.
		 	$this->view->plateno = $plateno ;//DISPLAY VALUE PLATE NO.********************
		 	$length= $this->request->getPost('length') ;//POST LENGTH
		 	$this->view->length = $length ;//DISPLAY VALUE LENGTH********************
		 	$width= $this->request->getPost('width') ;//POST WIDTH
		 	$this->view->width = $width ;//DISPLAY VALUE WIDTH********************
		 	$heigth= $this->request->getPost('heigth') ;//POST HEIGHT
		 	$this->view->heigth = $heigth ;//DISPLAY VALUE COMPANYHEIGHT********************
		 	//ADD SAVE NEW DATA ENTRY

		 	
		 	$add = new Trucks();
		 	$add->plateno 	= $plateno;
		 	$add->length 	= $length;
		 	$add->width 	= $width;
		 	$add->heigth 	= $heigth;
		 	$add->company 	= $company;
		 	if ($add->save() == false) {
		 		echo "Umh, We can store data: ";
		 		
		 	} else {
		 		$this->view->company = "" ;
       			$this->view->plateno = "" ;
        		$this->view->length = "" ;
        		$this->view->width = "" ;
        		$this->view->heigth = "" ;
		 		echo "Great, a new data was saved successfully!";
		 	}
        }
	}
	public function truckListAction($num="",$page="",$keyword="")
	{
		$this->view->setVar('header_title', "Truck Lists");

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if(!isset($_GET["page"])){
         $currentPage=0;
        }else{
            $currentPage = (int) $_GET["page"];
        }
        // The data set to paginate
        $company      = Trucks::find();
        $paginator   = new PaginatorModel(
            array(
                "data"  => $company,
                "limit" => 5,
                "page"  => $currentPage
                )
            );
        $this->view->page= $paginator->getPaginate();


        function display($model,$column){
            $sand = $model::find();
            $data = array();
            foreach ($sand as $m) {
                $data[] = array(
                    "id" => $m->id,
                    $column => $m->$column,
                );
            }
            return json_decode(json_encode($data));
        }
        $this->view->data_company = display("Company","campname"); //LIST SAND TYPE

	}

}
