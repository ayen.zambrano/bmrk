%{Phalcon_Http_Response_Cookies_c7196dcac59eeac64e9a48bd3a4a1d1d}%
==========================================

%{Phalcon_Http_Response_Cookies_dae0768c49bb93d396c216d3bf314c0a|:doc:`Phalcon\\Http\\Response\\CookiesInterface <Phalcon_Http_Response_CookiesInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Http_Response_Cookies_a66f270af120310921085fcc55698d3e}%

%{Phalcon_Http_Response_Cookies_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Http_Response_Cookies_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Http_Response_Cookies_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Http_Response_Cookies_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Http_Response_Cookies_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Http_Response_Cookies_e2a7d71c99adb8b35d0954e317c600ed|:doc:`Phalcon\\Http\\Response\\Cookies <Phalcon_Http_Response_Cookies>`}%

%{Phalcon_Http_Response_Cookies_f93e029d1a508211a1359fc53e021a06}%

%{Phalcon_Http_Response_Cookies_86c89b47d439c046fef3fb56cf6624cc}%

%{Phalcon_Http_Response_Cookies_18ad3094a620d63b4ce321fc397f61f5}%

%{Phalcon_Http_Response_Cookies_869895cc3b0cea18b8e2259a19ecb33b|:doc:`Phalcon\\Http\\Response\\Cookies <Phalcon_Http_Response_Cookies>`}%

%{Phalcon_Http_Response_Cookies_718629b163d14819d2b74de691177fe7}%

%{Phalcon_Http_Response_Cookies_ea2a951db2f6d2fa3ad9c9118694ae2a|:doc:`Phalcon\\Http\\Cookie <Phalcon_Http_Cookie>`}%

%{Phalcon_Http_Response_Cookies_0786ab248c3444ef059ddc516dc10ede}%

%{Phalcon_Http_Response_Cookies_3b3b383f1dfee3a85fb1212e54de5f65}%

%{Phalcon_Http_Response_Cookies_193298756414373eb1f9055ec2600a9f}%

%{Phalcon_Http_Response_Cookies_cc71993705dc4015f45c2096b7b69e40}%

%{Phalcon_Http_Response_Cookies_b53e08548df2f95460b80598c1a3aa25}%

%{Phalcon_Http_Response_Cookies_3be632de05ece6ff5f4a3f67c7ce06e8}%

%{Phalcon_Http_Response_Cookies_ceec68621cfe089658126ade64ffe326}%

%{Phalcon_Http_Response_Cookies_ef71b1b1c7013607711f18aad1f4a52e|:doc:`Phalcon\\Http\\Response\\Cookies <Phalcon_Http_Response_Cookies>`}%

%{Phalcon_Http_Response_Cookies_983c3ba1ba35e51a6a0d4eb98ffa0e47}%

