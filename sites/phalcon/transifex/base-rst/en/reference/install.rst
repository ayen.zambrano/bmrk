%{install_c0277e63a218ee84b1414b2c467dfbe2}%
============
%{install_c872ae13f816fd7dfbfc1f7781ddd782}%

%{install_8458efd5cfb7e32b7a48fe0ab64285af}%
-------
%{install_ab48ea9d057ab477b2520afc16b2c915|download_ }%

.. code-block:: bash

    extension=php_phalcon.dll


%{install_6a49732434aa0c102cc05c6a55e1879f}%

%{install_94b7a1e85a5e5275e9d2cc04f8023598}%

.. raw:: html

    <div align="center"><iframe src="http://player.vimeo.com/video/40265988" width="500" height="266" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>




%{install_77dcb6d601fa329e438e058425acf410|script_ }%

%{install_7a752ac7821139893d4b7d8eb762a483}%
^^^^^^^^^^^^^^
.. toctree::
    :maxdepth: 1

    xampp
    wamp



%{install_3ccb2aae6c01792ec77ea33834f928a7}%
-------------
%{install_14904f04d6b7f4d7b934098f29da804e}%

%{install_72a3660e8fa5fbfc8034c2e330a6f573}%
^^^^^^^^^^^^
%{install_70a4fa2ec4a0a6356164c746dc431647}%

* {%install_5a0e7d9ecd138223bbe7cb86b0598394%}
* {%install_d900b4cfd8fa6ae4dcb8c70c068b641b%}
* {%install_c4bd9f7964fd692c23f751f60b83c4eb%}

%{install_6e2d310434cb02d415fcff3819ca2f0b}%

.. code-block:: bash

    #Ubuntu
    sudo apt-get install php5-dev libpcre3-dev gcc make php5-mysql

    #Suse
    sudo yast -i gcc make autoconf2.13 php5-devel php5-pear php5-mysql

    #CentOS/RedHat/Fedora
    sudo yum install php-devel pcre-devel gcc make

    #Solaris
    pkg install gcc-45 php-53 apache-php53


%{install_9bae46c938052c986789bce50ddc11c1}%
^^^^^^^^^^^
%{install_7277a5ce5aea3f76cacc10e144264e7f}%

.. code-block:: bash

    git clone --depth=1 git://{%install_33fb7441b040e39278820011290bc5c4%}
    cd cphalcon/build
    sudo ./install


%{install_ddf369d0a475a6c8b618f59910f37ec7}%

.. code-block:: bash

    #Suse: Add this line in your php.ini
    extension=phalcon.so

    #Centos/RedHat/Fedora: Add a file called phalcon.ini in /etc/php.d/ with this content:
    extension=phalcon.so

    #Ubuntu/Debian: Add a file called 30-phalcon.ini in /etc/php5/conf.d/ with this content:
    extension=phalcon.so

    #Debian with php5-fpm: Add a file called 30-phalcon.ini in /etc/php5/fpm/conf.d/30-phalcon.ini with this content:
    extension=phalcon.so


%{install_c6ca645f3bcde844f8ec6f8c99f61167}%

%{install_bdb0f91a6cac541e48b89dc170f60a78}%

.. code-block:: bash

    sudo service php5-fpm restart


%{install_10d3ab0887312140a685f66b49109538}%

.. code-block:: bash

    cd cphalon/build
    sudo ./install 32bits
    sudo ./install 64bits
    sudo ./install safe


%{install_4fa7f33de3c42e85a5758573716e1446}%

.. code-block:: bash

    cd cphalcon/build/64bits
    export CFLAGS="-O2 --fvisibility=hidden"
    ./configure --enable-phalcon
    make && sudo make install


%{install_480e8a3dbdedef0d265ec3f0022ed1d1}%
--------
%{install_6398995d033e982e0831dd8f44f6d492}%

%{install_72a3660e8fa5fbfc8034c2e330a6f573}%
^^^^^^^^^^^^
%{install_70a4fa2ec4a0a6356164c746dc431647}%

* {%install_5a0e7d9ecd138223bbe7cb86b0598394%}
* {%install_8156c7f6289dda32268b8833e3809369%}

.. code-block:: bash

    #brew
    brew tap homebrew/homebrew-php
    brew install php53-phalcon
    brew install php54-phalcon
    brew install php55-phalcon
    brew install php56-phalcon

    #MacPorts
    sudo port install php53-phalcon
    sudo port install php54-phalcon
    sudo port install php55-phalcon
    sudo port install php56-phalcon


%{install_ddf369d0a475a6c8b618f59910f37ec7}%

%{install_b2f5cb7e2747e60a61390100a08e8376}%
-------
%{install_d43da86ea091d3cd812a33429d570ae8}%

.. code-block:: bash

    pkg_add -r phalcon


%{install_6899336ad02d327ac9b4d1670f72a18c}%

.. code-block:: bash

    export CFLAGS="-O2 --fvisibility=hidden"
    cd /usr/ports/www/phalcon && make install clean


%{install_23a4a0eeaa8c65b1110e8da55a261fe4}%
------------------
%{install_4c62f4f5d780cac9af64cb12a7120d75}%

