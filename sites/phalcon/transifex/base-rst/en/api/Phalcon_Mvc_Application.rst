%{Phalcon_Mvc_Application_ee204df8c72fbea6756a1e1e7ec06f68}%
===================================

%{Phalcon_Mvc_Application_32f67d09941ad6b6963c440e030d031e|:doc:`Phalcon\\DI\\Injectable <Phalcon_DI_Injectable>`}%

%{Phalcon_Mvc_Application_cb143a03062fad8680104c800ead4e79|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_Application_9108c3ef69128ededce7afe5231de9a8}%

.. code-block:: php

    <?php

     class Application extends \Phalcon\Mvc\Application
     {
    
    	/\**
    	 * Register the services here to make them general or register
    	 * in the ModuleDefinition to make them module-specific
    	 */
    	protected function _registerServices()
    	{
    
    	}
    
    	/\**
    	 * This method registers all the modules in the application
    	 */
    	public function main()
    	{
    		$this->registerModules(array(
    			'frontend' => array(
    				'className' => 'Multiple\Frontend\Module',
    				'path' => '../apps/frontend/Module.php'
    			),
    			'backend' => array(
    				'className' => 'Multiple\Backend\Module',
    				'path' => '../apps/backend/Module.php'
    			)
    		));
    	}
    }
    
    $application = new Application();
    $application->main();




%{Phalcon_Mvc_Application_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Application_cffb7f53d656db7c908ca993a7a752f2|:doc:`Phalcon\\DI <Phalcon_DI>`}%

%{Phalcon_Mvc_Application_9b722f866edec3bd43425ad4c6c2b2aa|:doc:`Phalcon\\Mvc\\Application <Phalcon_Mvc_Application>`}%

%{Phalcon_Mvc_Application_cce3a1145694a6ec1539fab4fd4c5354}%

%{Phalcon_Mvc_Application_b280af0d6f4a9ae23f7227fd55ba2b21}%

%{Phalcon_Mvc_Application_4162233ffec3547ad7610e1cf880a645}%

.. code-block:: php

    <?php

    $this->registerModules(array(
    	'frontend' => array(
    		'className' => 'Multiple\Frontend\Module',
    		'path' => '../apps/frontend/Module.php'
    	),
    	'backend' => array(
    		'className' => 'Multiple\Backend\Module',
    		'path' => '../apps/backend/Module.php'
    	)
    ));





%{Phalcon_Mvc_Application_2b65900c295fb31cc1c4d306b08bd796}%

%{Phalcon_Mvc_Application_b63d73065dd34e5b4fc9332470548b81}%

%{Phalcon_Mvc_Application_d6c39c442b1aa99130e3689f731414ff|:doc:`Phalcon\\Mvc\\Application <Phalcon_Mvc_Application>`}%

%{Phalcon_Mvc_Application_b41900be79ad3b54d459d554cd52e8f5}%

%{Phalcon_Mvc_Application_26e3314787194c948c848fba815d43c9}%

%{Phalcon_Mvc_Application_f1e3b519223259bb2eed3c15483a13d6}%

%{Phalcon_Mvc_Application_870af55f145f6d91a88cdef0efe25a9e|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Mvc_Application_bf0ed83dec2b2390f4ee65a2245fb6ec}%

%{Phalcon_Mvc_Application_d98a2143a4e9b08520a7df0009a78266|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Application_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Mvc_Application_3e7e4ddacc724c78120150572bdab704|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Application_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Mvc_Application_5e6f785f2df62ae1a2a672663fb6b2e4|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Application_994924f85ea614b243b7b4d954e37a48}%

%{Phalcon_Mvc_Application_8d859c2f0efce8383f08ae517e34a4af|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Application_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_Mvc_Application_aa385da180999c3488ab22e043b6eb9d}%

%{Phalcon_Mvc_Application_a8ec8322461cb1dce1953c9378484594}%

