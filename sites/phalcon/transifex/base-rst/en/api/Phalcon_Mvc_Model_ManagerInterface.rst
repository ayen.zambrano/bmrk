%{Phalcon_Mvc_Model_ManagerInterface_862025ecb078d24337c77b381cb69701}%
===================================================

%{Phalcon_Mvc_Model_ManagerInterface_125750306bce79f3235b8795353461c5}%

%{Phalcon_Mvc_Model_ManagerInterface_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_ManagerInterface_45fbc7cbf41abfc375ef9668f8c61069|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_07292c08f64ef28e5aa076b3eec53ad2}%

%{Phalcon_Mvc_Model_ManagerInterface_df5363a1c5f162a5803d78a02c03fd07}%

%{Phalcon_Mvc_Model_ManagerInterface_ca2af7e044a9e9e1c6febad635ee8340}%

%{Phalcon_Mvc_Model_ManagerInterface_b9e437a26dffc049959f5dbece8a9763|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_194105413cb46513c240f5424d73bca3}%

%{Phalcon_Mvc_Model_ManagerInterface_3ad884f978d69157c20ee03ff3edf110|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_953e0302618775be05eb68f805da053b}%

%{Phalcon_Mvc_Model_ManagerInterface_c91177330f8c69b969cab42a565fe565|:doc:`Phalcon\\Mvc\\Model\\RelationInterface <Phalcon_Mvc_Model_RelationInterface>`|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_5ba5980d7cf29307f28789f5d949aae5}%

%{Phalcon_Mvc_Model_ManagerInterface_bbd1698c8eebd54b95bd63df895adfdc|:doc:`Phalcon\\Mvc\\Model\\RelationInterface <Phalcon_Mvc_Model_RelationInterface>`|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_3a9d0428bef32492161ef95eefee0da6}%

%{Phalcon_Mvc_Model_ManagerInterface_d94e52492e025adb6c6dffc423e81d5a|:doc:`Phalcon\\Mvc\\Model\\RelationInterface <Phalcon_Mvc_Model_RelationInterface>`|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_ad343ba2a8633b9021565dec5a91cb49}%

%{Phalcon_Mvc_Model_ManagerInterface_53291a4d02200270fb54b1ab8d8bb8fb}%

%{Phalcon_Mvc_Model_ManagerInterface_2d18b3d6036b432853d0dd6ddb56db8a}%

%{Phalcon_Mvc_Model_ManagerInterface_4a6c8129c85817827d693bb35ea4e24c}%

%{Phalcon_Mvc_Model_ManagerInterface_78a85feda45d2f9136d364dd5365fd0e}%

%{Phalcon_Mvc_Model_ManagerInterface_04e9244dc112f4de6ce86ae114ba1433}%

%{Phalcon_Mvc_Model_ManagerInterface_c6363e57e5b26e1a5e13b3f749e9b00b}%

%{Phalcon_Mvc_Model_ManagerInterface_10a95d61cab4eb651affb7d58a1372b0|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_ManagerInterface_ceed518a7eeb5fda30e4516f6229fe1e}%

%{Phalcon_Mvc_Model_ManagerInterface_ceedb731a9b0f18647a995bbed94c858|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_ManagerInterface_b3d8f020bc868a134c401da8a9f13237}%

%{Phalcon_Mvc_Model_ManagerInterface_74d04680840661698eb7ff7c0ed1a47e|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_ManagerInterface_ceed518a7eeb5fda30e4516f6229fe1e}%

%{Phalcon_Mvc_Model_ManagerInterface_bebcc9f7846654d2f5e6e29260d2dbe9|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_40cdb86a2f1e4689231d740da52462d5}%

%{Phalcon_Mvc_Model_ManagerInterface_c351d75f68be68eac24d80aaaec1165b|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_5da033024213362b4d5299b7f4e112b2}%

%{Phalcon_Mvc_Model_ManagerInterface_25b7b3021b173d34f12ac2046a377159|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_4342059e9b0efd979e30f16588d43717}%

%{Phalcon_Mvc_Model_ManagerInterface_210c93cf1b1a85a4dbad09042863bdcc|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_4342059e9b0efd979e30f16588d43717}%

%{Phalcon_Mvc_Model_ManagerInterface_85f7096561c2a056e4343eb78dea0b87|:doc:`Phalcon\\Mvc\\Model\\RelationInterface <Phalcon_Mvc_Model_RelationInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_41ca42d487202952891e816a2b2dccd4}%

%{Phalcon_Mvc_Model_ManagerInterface_12e4366123deed76dc68abc2279914da}%

%{Phalcon_Mvc_Model_ManagerInterface_1a3faf60f026a52c625be7b1b5a20d8d}%

%{Phalcon_Mvc_Model_ManagerInterface_212275a9370a7a9fbcb8d997d522acc9|:doc:`Phalcon\\Mvc\\Model\\QueryInterface <Phalcon_Mvc_Model_QueryInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_c4888a8c5c5b6d87167aa38f2e660331}%

%{Phalcon_Mvc_Model_ManagerInterface_a4bed18865ec4b4a410a6148271a734d|:doc:`Phalcon\\Mvc\\Model\\QueryInterface <Phalcon_Mvc_Model_QueryInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_567b3a0b16e3ab59deebe44f90a148d1}%

%{Phalcon_Mvc_Model_ManagerInterface_502e1342516a574a1389865d4913f31c|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_d2afc31533b1a6f2d20e8223042b77d6}%

%{Phalcon_Mvc_Model_ManagerInterface_3f393fc0a35612f97c896a76a57ded28|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`|:doc:`Phalcon\\Mvc\\Model\\BehaviorInterface <Phalcon_Mvc_Model_BehaviorInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_0993a2571de7624c1ba1bb2e000c261b}%

%{Phalcon_Mvc_Model_ManagerInterface_93d8f8beae93d935f8710018c6e55f7e|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_5ab646fcc0177d33b88cec54073f262e}%

%{Phalcon_Mvc_Model_ManagerInterface_ada3807caba55eeff985725a52f678b3|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_6927ac17a3971944b4ec96376e7287e1}%

%{Phalcon_Mvc_Model_ManagerInterface_2d1ec23810600dda698db7d8d514e6e6|:doc:`Phalcon\\Mvc\\Model\\QueryInterface <Phalcon_Mvc_Model_QueryInterface>`}%

%{Phalcon_Mvc_Model_ManagerInterface_469b5233644a0b2eba44d64e5372af78}%

