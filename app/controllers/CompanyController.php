<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Mvc\Model\Query;

class CompanyController extends Controller
{
	public function initialize(){
		$this->view->setTemplateAfter('template');
	}

	public function session(){
		if(!isset($_SESSION["admin"])){
			header('Location: ../login');
		}
	}


	public function addcompanyAction()
	{
		// $this->session();
		$this->view->setVar('header_title', "Add New Company");
		//if ($this->request->isPost('save') == true) {
		 	//VARIABLE
		//	$compname= $this->request->getPost('compname');
		//	$address= $this->request->getPost('address') ;
		//	$contact= $this->request->getPost('contact') ;
		 	//ADD SAVE NEW DATA ENTRY
		//	$add = new Company();
		//	$add->campname 	= $compname;
		//	$add->address 	= $address;
		//	$add->contact 	= $contact;

		//}
		$data = json_decode(file_get_contents("php://input"));
		if(count($data) > 0){
		$compname = $data->compname;
		$address = $data->address;
		$contact = $data->contact;

		$add = new Company();
		$add->campname 	= $compname;
		$add->address 	= $address;
		$add->contact 	= $contact;

			if ($add->save() == false) {
				echo "Umh, We can't store data: ";
				foreach ($robot->getMessages() as $message) {
					echo $message;
				}
			} else {
				echo "Great, a new data was saved successfully!";
			}
		}
	}

	public function editcompanyAction($id)
	{
		
		$this->view->setVar('header_title', "Edit Company Info");
		//$company      = Company::find(array("id='".$id."'"));
		//$this->view->companyname= $company[0]->campname;
		//$this->view->address= $company[0]->address ;
		//$this->view->contact= $company[0]->contact ;
		//echo json_encode($company);

		$table_company = Company::find(array("id='".$id."'"));
        foreach ($table_company as $m) {
            $data[] = array(
            	'id' => $m->id ,
                'campname' => $m->campname ,
                'address' => $m->address ,
                'contact' => $m->contact ,
                );
        }
        $this->view->m= json_decode(json_encode($data));

		if ($this->request->isPost('update') == true) {
		 	//VARIABLE
			$compname= $this->request->getPost('compname');
			$address= $this->request->getPost('address') ;
			$contact= $this->request->getPost('contact') ;
		 	//ADD SAVE NEW DATA ENTRY
			$add = Company::findFirst('id='.$id.'');
			$add->campname 	= $compname;
			$add->address 	= $address;
			$add->contact 	= $contact;

			if ($add->save() == false) {
				echo "Umh, We can't store data: ";
				foreach ($robot->getMessages() as $message) {
					echo $message;
				}
			} else {
				 header('Location: ../compList');
			}
		}
		
	}

	public function compListAction($num="",$page="",$keyword="")
	{
		$this->view->setVar('header_title', "Client Company List");
		$add = new Company();
		if(!isset($_GET["page"])){
			$currentPage=0;
		}else{
			$currentPage = (int) $_GET["page"];
		}

		if ($this->request->isPost('search') == true) {
			$search_q= $this->request->getPost('query');
			$query  = $this->modelsManager->createQuery("SELECT * FROM company WHERE company.campname LIKE '%$search_q%' or company.address LIKE '%$search_q%'  ");
			$company      = $query->execute();	
		}else{
			$query  = $this->modelsManager->createQuery("SELECT * FROM company  ");
			$company      = $query->execute();
		}
		 
		

		// Create a Model paginator, show 10 rows by page starting from $currentPage
		$paginator   = new PaginatorModel(
			array(
				"data"  => $company,
				"limit" =>20,
				"page"  => $currentPage
				)
			);

		// Get the paginated results
		$this->view->page= $paginator->getPaginate();


	}

	public function compprofileAction($id)
	{
		$this->view->setVar('header_title', "Company Profile");
		$this->view->compid= $id;

		$table_company = Company::find(array("id='".$id."'"));
        foreach ($table_company as $m) {
            $data[] = array(
            	'id' => $m->id ,
                'campname' => $m->campname ,
                'address' => $m->address ,
                'contact' => $m->contact ,
                );
        }
        $this->view->m= json_decode(json_encode($data));

		////////////////////////LIST OWNED TRUCKS
		if(!isset($_GET["page"])){
			$currentPage=0;
		}else{
			$currentPage = (int) $_GET["page"];
		}
        // The data set to paginate
		$truck      = Trucks::find(array("company='".$id."'"));
		$paginator   = new PaginatorModel(
			array(
				"data"  => $truck,
				"limit" => 20,
				"page"  => $currentPage
				)
			);
		$this->view->page= $paginator->getPaginate();



	}

	public function orderedhistoryAction($id)
	{
		$this->view->setVar('header_title', "Company Profile");
		$this->view->compid= $id;

		$company      = Company::find(array("id='".$id."'"));
		$this->view->companyname= $company[0]->campname;
		$this->view->address= $company[0]->address ;
		$this->view->contact= $company[0]->contact ;

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        /////DAILY REPORT
         $this->view->from ="";//DISPLAY VALUE from********************
         $this->view->to ="";//DISPLAY VALUE from********************
         $this->view->dspval ="";//DISPLAY VALUE from********************
         $this->view->disabled = "disabled" ;//DISABLED BOTTN********************
         $this->view->note = "" ;//Notification********************
         if ($this->request->isPost('showrpt') == true) {
     		 
         	$from= $this->request->getPost('from');//POST from
		 	$this->view->from = $from ;//DISPLAY VALUE from********************
		 	$to= $this->request->getPost('to');//POST to
		 	$this->view->to = $to ;//DISPLAY VALUE to********************

		 	 $this->view->dspval ="displaydaily";//DISPLAY VALUE from********************

		 	// echo $from ."<br>". $to."<br>";

		 	if($from>$to){
		 		$this->view->note = "To Date Must Be Greater than From Date" ;
		 		$this->view->dborder= json_decode(json_encode($data_orders=array()));
		 	}else{
		 		$this->view->disabled = "";
		 		$query  = $this->modelsManager->createQuery("SELECT * FROM orders WHERE orders.company='$id' and orders.date >= '$from' and orders.date <= '$to'");
				$orderdate   = $query->execute();
				

		 		// $orderdate=Orders::find(array($condition));
		 		// // echo count($orderdate);
		 		foreach ( $orderdate as $daily){
		 			$data_orders[] = array(
					'plateno'=> $daily->plateno ,
					'company' => $daily->company,
					'sand' => $daily->sand,
					'cumtr' => $daily->cumtr,
					'pricecumtr' => $daily->pricecumtr,
					'cash'=> $daily->cash ,
					'poamount'=> $daily->poamount ,
					'discount'=> $daily->discount ,
					'drno'=> $daily->drno ,
					'time'=> $daily->time ,
					'date'=> $daily->date ,				);
		 		}
		 		$this->view->dborder= json_decode(json_encode($data_orders));
		 	}

         }else{
         		
         	$this->view->dborder= json_decode(json_encode($data_orders=array()));
         }
	}


	public function setproductpriceAction($id)
	{
		$this->view->setVar('header_title', "Company Profile");
		$this->view->compid= $id;

		$company      = Company::find(array("id='".$id."'"));
		$this->view->companyname= $company[0]->campname;
		$this->view->address= $company[0]->address ;
		$this->view->contact= $company[0]->contact ;

		////////////////////////LIST PRODUCT
		$currentsetdate=Setpricedate::find();/////////////////////////////////////
		foreach ($currentsetdate as $price) {
			$curdate =  $price->date;
			
		}
		$sand   = Sand::find(array("date='".$curdate."'"));
		foreach ($sand as $m) {
			$data[] = array(
				'id' => $m->id ,
				'sandtype' => $m->sandtype,
				'sandcateg' => $m->sandcateg,
				'price' => $m->price,
				);
		}
		$this->view->sand= json_decode(json_encode($data));

		///////////////////////////////////////////////////
		function display($model,$column){
			$sand = $model::find();
			$data = array();
			foreach ($sand as $m) {
				$data[] = array(
					'id' => $m->id ,
					$column => $m->$column,
					);
			}
			return json_decode(json_encode($data));
		}

		function setprice($id){
			$setprice = Setprice::find(array("company='".$id."'"));
			$data = array();
			foreach ($setprice as $m) {
				$data[] = array(
					"sand"=> $m->sand,
					"setprice"=> $m->setprice,
					);
			}
			return json_decode(json_encode($data));
		}

		$this->view->data_type = display("Sandtype","type"); //LIST SAND TYPE
		$this->view->data_categ = display("Sandcateg","category"); //LIST SAND CATEGORY
		$this->view->set_price= setprice($id); //LIST SAND CATEGORY





		/////////////////////////////////////////////////SAVE
		if ($this->request->isPost('save') == true) {
			$checked_count = count($_POST['sandid']);
			$sandid = $_POST['sandid'];

			$price = $_POST['price'];

			for($i=0;$i<=$checked_count;$i++){


				if(@$price[$i]!=""){
		 			//Check If Exist
					$setprice= Setprice::find(array("company='".$id."' and  sand='".@$sandid[$i]."'"));
					if(count($setprice)==0){

						$add = new Setprice();
						$add->company 	= $id;
						$add->sand 		= @$sandid[$i];
						$add->setprice 	= @$price[$i];

						if ($add->save() == false) {
							echo "Umh, We can store data: ";
							foreach ($add->getMessages() as $message) {
								echo $message;
							}
						} else {
							echo "Great, a new data was saved successfully!";
						}
					}else{

						$update = Setprice::findFirst(array("company='".$id."' and  sand='".@$sandid[$i]."'"));
						$update->setprice = @$price[$i];

						if ($update->save() == false) {
							echo "Umh, We can store data: ";
							foreach ($update->getMessages() as $message) {
								echo $message;
							}
						} else {
							echo "Great, a new data was saved successfully!";
						}
					}
				}
			}
		}
	}
	public function dltcompanyAction($id)
	{


		$this->view->setVar('header_title', "Company Profile");
		$this->view->compid= $id;

		//$company      = Company::find(array("id='".$id."'"));
		//$this->view->companyname= $company[0]->campname;
		//$this->view->address= $company[0]->address ;
		//$this->view->contact= $company[0]->contact ;
		$table_company = Company::find(array("id='".$id."'"));
        foreach ($table_company as $m) {
            $data[] = array(
            	'id' => $m->id ,
                'campname' => $m->campname ,
                'address' => $m->address ,
                'contact' => $m->contact ,
                );
        }
        $this->view->m= json_decode(json_encode($data));

		if ($this->request->isPost('delete') == true) {

			    $dltPhoto = Company::findFirst('id='.$id.' ');
		        $data = array('error' => 'Not Found');
		        if ($dltPhoto) {
		            if($dltPhoto->delete()){
		               header('Location: ../compList');
		            }
		        }

		}



		
	}
	

}
