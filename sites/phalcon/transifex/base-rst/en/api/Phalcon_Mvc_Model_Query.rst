%{Phalcon_Mvc_Model_Query_50fce70d3b4b4cdf4f5e9380a6dd8fa5}%
====================================

%{Phalcon_Mvc_Model_Query_c3c6d124987bee5458b418523ac892f1|:doc:`Phalcon\\Mvc\\Model\\QueryInterface <Phalcon_Mvc_Model_QueryInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_Model_Query_cc269403cc36b5a9d528900cc59ed0ce}%

.. code-block:: php

    <?php

     $phql = "SELECT c.price*0.16 AS taxes, c.* FROM Cars AS c JOIN Brands AS b
              WHERE b.name = :name: ORDER BY c.name";
    
     $result = $manager->executeQuery($phql, array(
       'name' => 'Lamborghini'
     ));
    
     foreach ($result as $row) {
       echo "Name: ", $row->cars->name, "\n";
       echo "Price: ", $row->cars->price, "\n";
       echo "Taxes: ", $row->taxes, "\n";
     }




%{Phalcon_Mvc_Model_Query_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Mvc_Model_Query_6594744045d390021827e36d5d260b5f}%

%{Phalcon_Mvc_Model_Query_53419f360989fa16771acee9f51ce6a2}%

%{Phalcon_Mvc_Model_Query_9877d4f07424b8626270c0480b28687c}%

%{Phalcon_Mvc_Model_Query_6d6876681abd7833c109f06755eb7606}%

%{Phalcon_Mvc_Model_Query_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Query_a80cfcef38dec18df62c24a5b4aebe39}%

%{Phalcon_Mvc_Model_Query_d288f74e89163499a59bc969c7dd6a0b}%

%{Phalcon_Mvc_Model_Query_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Query_992eb21f94076f486939b432849d799c}%

%{Phalcon_Mvc_Model_Query_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Query_1bac54934bec20940b708d05267cfd49}%

%{Phalcon_Mvc_Model_Query_edeacc25b9fc5a945b48255b1ccceac5|:doc:`Phalcon\\Mvc\\Model\\Query <Phalcon_Mvc_Model_Query>`}%

%{Phalcon_Mvc_Model_Query_f3c7bd96ecf41998a94ff4c200f3e5cb}%

%{Phalcon_Mvc_Model_Query_c5d1312f247b5c500c76b9ed13cb52d0}%

%{Phalcon_Mvc_Model_Query_e2c5d7e6f7ed8f314ff6d74fdfeca2c1}%

%{Phalcon_Mvc_Model_Query_cd037eca844d717f611cf9f7cf465a00}%

%{Phalcon_Mvc_Model_Query_f0de72b05c1713ad0836fffcf47906c6}%

%{Phalcon_Mvc_Model_Query_36d551205df83caad921f157b1ef3b6b}%

%{Phalcon_Mvc_Model_Query_049fda272db3565ce96a76d93643bae5}%

%{Phalcon_Mvc_Model_Query_0dcd36fa78046363e77aa515d4d0f839}%

%{Phalcon_Mvc_Model_Query_049fda272db3565ce96a76d93643bae5}%

%{Phalcon_Mvc_Model_Query_e27d08e1647fa29cf1aa30886ca7c3a5}%

%{Phalcon_Mvc_Model_Query_65a54fdb64ce485409b73e6211a472e0}%

%{Phalcon_Mvc_Model_Query_90be665839d45bda5cc1c0a40ddbf2ed}%

%{Phalcon_Mvc_Model_Query_c16d8caa78e5b7f6cfd5c1c4f4adcb58}%

%{Phalcon_Mvc_Model_Query_3412631e6730ef7a90d725dc12a49830}%

%{Phalcon_Mvc_Model_Query_29b21d5f7c19ce011186748597a28d9c}%

%{Phalcon_Mvc_Model_Query_4e326453d49251662a9ece50bfdbd602}%

%{Phalcon_Mvc_Model_Query_c0d19903f1a4af956871c8b81e009791}%

%{Phalcon_Mvc_Model_Query_b04adaf81df416e1caa8ca9b2b352009}%

%{Phalcon_Mvc_Model_Query_5eec27419990017549381b79dc2980cd}%

%{Phalcon_Mvc_Model_Query_0d50534c9857de6a451a74ebf7dfaf67}%

%{Phalcon_Mvc_Model_Query_825f663fdcb980cf70e2d821e4791f5f}%

%{Phalcon_Mvc_Model_Query_8a10bdcba1c8d44b1fd5c359d4842a2d}%

%{Phalcon_Mvc_Model_Query_6b5d52c5825b8f8fe2f7ca6c845af917}%

%{Phalcon_Mvc_Model_Query_b1db21464630c1501041bc4a85f0574f}%

%{Phalcon_Mvc_Model_Query_14702f4772ac96ee000e551252058145}%

%{Phalcon_Mvc_Model_Query_6c91670cce1cfa6553e86662f77a73a6}%

%{Phalcon_Mvc_Model_Query_67d5568ac230fe01b1ae571d90381197}%

%{Phalcon_Mvc_Model_Query_0b178ac32c4157bf60a589cb64b69d0e}%

%{Phalcon_Mvc_Model_Query_af7403929ad0a5d2040bf73e3df70207}%

%{Phalcon_Mvc_Model_Query_92023ad23c40a208d6a36becb6e16c15}%

%{Phalcon_Mvc_Model_Query_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Mvc_Model_Query_b9b86ad1b1058e67f521881be9b87d41}%

%{Phalcon_Mvc_Model_Query_e7d6afcdbefaad78fdeb31ebd1a55592}%

%{Phalcon_Mvc_Model_Query_157c48b344c8e6d62085fec74cff649f}%

%{Phalcon_Mvc_Model_Query_d1560e6b868c8285f02b70eb5d5fd8e4}%

%{Phalcon_Mvc_Model_Query_31130e709d64f0444bc735d6dceac5c2}%

%{Phalcon_Mvc_Model_Query_a99e1f816a9588509b2599c1d90fb6af}%

%{Phalcon_Mvc_Model_Query_09a212d121f55ef39afe67efa1fd6fc1}%

%{Phalcon_Mvc_Model_Query_99c0a436bcc14a187b976deb8a33844b}%

%{Phalcon_Mvc_Model_Query_3edfd425604f284d49f67a03b667a22c}%

%{Phalcon_Mvc_Model_Query_d2b9d66af704b021286bf5c7c4e061ab}%

%{Phalcon_Mvc_Model_Query_bc7eb5c9a8263ce418b8d569cb54c22b|:doc:`Phalcon\\Mvc\\Model\\Query <Phalcon_Mvc_Model_Query>`}%

%{Phalcon_Mvc_Model_Query_7972967a3d166b0f5ed7587ccac597c8}%

%{Phalcon_Mvc_Model_Query_5a122820f3601abce4f8e02ee19a9536}%

%{Phalcon_Mvc_Model_Query_ea8936d1e8b2b5e4c80ca22bd678371d}%

%{Phalcon_Mvc_Model_Query_09b5c9851ac944b212045804108afe45|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Mvc_Model_Query_847509083cf4215864ed931c4390db25}%

%{Phalcon_Mvc_Model_Query_25493de7ff611ebb9e536facfa717fe0|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`}%

%{Phalcon_Mvc_Model_Query_2cda7df4c4f33dea751d1c0ff794e46f}%

%{Phalcon_Mvc_Model_Query_7ca4144a4baba0fb122e06d2ade1425e|:doc:`Phalcon\\Mvc\\Model\\Query\\StatusInterface <Phalcon_Mvc_Model_Query_StatusInterface>`}%

%{Phalcon_Mvc_Model_Query_530d4ecfbd02acac07bc140ab75baa78}%

%{Phalcon_Mvc_Model_Query_7e30ce2fa221a8bf1383cc3725d8cd4f|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`}%

%{Phalcon_Mvc_Model_Query_78f86abd9832b87968c3ed144aa4d8cf}%

%{Phalcon_Mvc_Model_Query_0e3f0455367acfde0816518270df7d3e|:doc:`Phalcon\\Mvc\\Model\\Query\\StatusInterface <Phalcon_Mvc_Model_Query_StatusInterface>`}%

%{Phalcon_Mvc_Model_Query_6ba81c6d1aaf2d4b0c965c2037d79d77}%

%{Phalcon_Mvc_Model_Query_eaf13dbd9e37c5feadd4281963a5a02a|:doc:`Phalcon\\Mvc\\Model\\Query\\StatusInterface <Phalcon_Mvc_Model_Query_StatusInterface>`}%

%{Phalcon_Mvc_Model_Query_2ace1e57892481f0113070c9a02d5a26}%

%{Phalcon_Mvc_Model_Query_01035c9d8d895895f2a8238e19ff3b3a}%

%{Phalcon_Mvc_Model_Query_d52ed7c599d31f6fcdae90a73db9ab32}%

%{Phalcon_Mvc_Model_Query_207db2d1594146890e3563ec2c6296c5}%

%{Phalcon_Mvc_Model_Query_330cfb141577bc613e83742af646eea8}%

%{Phalcon_Mvc_Model_Query_b64ea56c80f5c2aba681d82cf9202e77|:doc:`Phalcon\\Mvc\\Model\\Query <Phalcon_Mvc_Model_Query>`}%

%{Phalcon_Mvc_Model_Query_35f7e47630f8c8b4c4407339e8419156}%

%{Phalcon_Mvc_Model_Query_476f2497c851eae8a78f73032ad317bb}%

%{Phalcon_Mvc_Model_Query_d5bd242121a981d3010c3d2d17232e69}%

%{Phalcon_Mvc_Model_Query_42649abcf6a4defbdf9eae0fca399317|:doc:`Phalcon\\Mvc\\Model\\Query <Phalcon_Mvc_Model_Query>`}%

%{Phalcon_Mvc_Model_Query_ed35b28467754fb2dc707f503a83b2fb}%

%{Phalcon_Mvc_Model_Query_9e9ac194ddb8c02caa0a82fe649938c5}%

%{Phalcon_Mvc_Model_Query_ccf0a2fab1417ed9e6fda9ec3421aa2f}%

%{Phalcon_Mvc_Model_Query_5fac1374daca826fc1324aca155c4a23|:doc:`Phalcon\\Mvc\\Model\\Query <Phalcon_Mvc_Model_Query>`}%

%{Phalcon_Mvc_Model_Query_ed35b28467754fb2dc707f503a83b2fb}%

%{Phalcon_Mvc_Model_Query_d6af3f9f34a56dd3ea3c9942bc827816}%

%{Phalcon_Mvc_Model_Query_debf454061e718509f6f06a6c38880b7}%

%{Phalcon_Mvc_Model_Query_5dd26627f354bc050cc113eed913b47d|:doc:`Phalcon\\Mvc\\Model\\Query <Phalcon_Mvc_Model_Query>`}%

%{Phalcon_Mvc_Model_Query_fb9da42446b249061f90c39fd2e4ab08}%

%{Phalcon_Mvc_Model_Query_bc42312436363d56005ed0f5740cddb2}%

%{Phalcon_Mvc_Model_Query_c4db1970a6b693cb5072fa1622f999cc}%

