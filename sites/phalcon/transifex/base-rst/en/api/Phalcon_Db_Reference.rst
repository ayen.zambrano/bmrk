%{Phalcon_Db_Reference_bd01e6c3623b8c8e604de3935899d3b8}%
================================

%{Phalcon_Db_Reference_6f26f6b53f53bf69629cd9c0ffb0c3b5|:doc:`Phalcon\\Db\\ReferenceInterface <Phalcon_Db_ReferenceInterface>`}%

%{Phalcon_Db_Reference_dd695b69098f7327edd79eea8f78963c}%

.. code-block:: php

    <?php

    $reference = new Phalcon\Db\Reference("field_fk", array(
    	'referencedSchema' => "invoicing",
    	'referencedTable' => "products",
    	'columns' => array("product_type", "product_code"),
    	'referencedColumns' => array("type", "code")
    ));




%{Phalcon_Db_Reference_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Db_Reference_756811608b58e5b95615d746c6ad1503}%

%{Phalcon_Db_Reference_6b04b1e4a60f405ee5390cdbd21dbc7e}%

%{Phalcon_Db_Reference_bad4450b78127e6c6b52db7413174386}%

%{Phalcon_Db_Reference_23ee79cb40a12ae4e9144e7a5a7e7046}%

%{Phalcon_Db_Reference_0874a2d6a938f656aaf55a3f04ff8f91}%

%{Phalcon_Db_Reference_9334f2f202f1de56dd82f96b16e3ba44}%

%{Phalcon_Db_Reference_92cc0e45936474764e1d05911cf7051f}%

%{Phalcon_Db_Reference_9334f2f202f1de56dd82f96b16e3ba44}%

%{Phalcon_Db_Reference_5f389c1fc3a7031907137fdb88505d7a}%

%{Phalcon_Db_Reference_a14ce3b53e874f65b677d4c0773302eb}%

%{Phalcon_Db_Reference_cdc775eff0e51e1076c3ed2cbd9147e7}%

%{Phalcon_Db_Reference_5b2032cc11d5896345c6c7e9a85661d1}%

%{Phalcon_Db_Reference_0c9c96176563fbf73f8ba6b17a03ef95}%

%{Phalcon_Db_Reference_ea01aeb291b3d095802f5bcfa51c3196}%

%{Phalcon_Db_Reference_b4729e3bc6effcb478c70a54add1e671|:doc:`Phalcon\\Db\\Reference <Phalcon_Db_Reference>`}%

%{Phalcon_Db_Reference_1038c71fdf56b2e89a165748b73b26c8}%

