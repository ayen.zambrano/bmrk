%{Phalcon_Mvc_Model_Transaction_02c41d7c11007e40a8d7b5d0948bd978}%
==========================================

%{Phalcon_Mvc_Model_Transaction_872e442de07331ebe2d5b29548cb76fe|:doc:`Phalcon\\Mvc\\Model\\TransactionInterface <Phalcon_Mvc_Model_TransactionInterface>`}%

%{Phalcon_Mvc_Model_Transaction_7dd48ce69bcd83415ab905eec460111e}%

.. code-block:: php

    <?php

    try {
    
      $manager = new Phalcon\Mvc\Model\Transaction\Manager();
    
      $transaction = $manager->get();
    
      $robot = new Robots();
      $robot->setTransaction($transaction);
      $robot->name = 'WALL·E';
      $robot->created_at = date('Y-m-d');
      if ($robot->save() == false) {
        $transaction->rollback("Can't save robot");
      }
    
      $robotPart = new RobotParts();
      $robotPart->setTransaction($transaction);
      $robotPart->type = 'head';
      if ($robotPart->save() == false) {
        $transaction->rollback("Can't save robot part");
      }
    
      $transaction->commit();
    
    } catch(Phalcon\Mvc\Model\Transaction\Failed $e) {
      echo 'Failed, reason: ', $e->getMessage();
    }




%{Phalcon_Mvc_Model_Transaction_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Transaction_ebeb0a83801cb497fe11dc38b6b58d27|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Transaction_fc8b25c9d17a52b859e98cd0a0ae19a6}%

%{Phalcon_Mvc_Model_Transaction_8e3f5a94fe257e6e7124709f5789b366|:doc:`Phalcon\\Mvc\\Model\\Transaction\\ManagerInterface <Phalcon_Mvc_Model_Transaction_ManagerInterface>`}%

%{Phalcon_Mvc_Model_Transaction_ea3db93b456728d20e714298e4ca3159}%

%{Phalcon_Mvc_Model_Transaction_fce129ec5f527a69545b5c56967b7fe2}%

%{Phalcon_Mvc_Model_Transaction_bff79e282b8671b90a6ad594e67c144b}%

%{Phalcon_Mvc_Model_Transaction_e16a188f253a999a42713dab713af4a5}%

%{Phalcon_Mvc_Model_Transaction_e056771a69c4eda4f4ad66240e2955eb}%

%{Phalcon_Mvc_Model_Transaction_96bff0977b044db803868299b0d9879f|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Transaction_0ef6aab116934b3059b3a2ce10906003}%

%{Phalcon_Mvc_Model_Transaction_70efa7432edb925e129cdb312bb9ea48|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`}%

%{Phalcon_Mvc_Model_Transaction_6919d085632f0c873cb84b2afe87a4d1}%

%{Phalcon_Mvc_Model_Transaction_d7fea9e659917a51ecaefc557e80a613}%

%{Phalcon_Mvc_Model_Transaction_f5881204525b0ebff024cccca2ba1d51}%

%{Phalcon_Mvc_Model_Transaction_5d732e23d325547cb17c8f986e8c52db}%

%{Phalcon_Mvc_Model_Transaction_200807cd22934c89548f0f79dadd1b39}%

%{Phalcon_Mvc_Model_Transaction_41f7ebb8689c485f0a2aafe91fb4241e}%

%{Phalcon_Mvc_Model_Transaction_65519ff7fb0a0fdbdeb36c7ac2eeaff1}%

%{Phalcon_Mvc_Model_Transaction_f80bc8026cd0e60b933a010a52a3c763}%

%{Phalcon_Mvc_Model_Transaction_980e5df422d01dc3b06f345648706351}%

%{Phalcon_Mvc_Model_Transaction_4a0bf27f23cf713fbb9fb0ced4ec860a}%

%{Phalcon_Mvc_Model_Transaction_8078cf850889cf9d7414d074f298bc8f}%

%{Phalcon_Mvc_Model_Transaction_a112607340714f339d047be430b5b8dd|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Transaction_5cf15634e6ff82de122f69c1038bc677}%

