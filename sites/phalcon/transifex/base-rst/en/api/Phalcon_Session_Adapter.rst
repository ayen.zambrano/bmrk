%{Phalcon_Session_Adapter_9c50dc7d4c51675417bd065f4e8c349d}%
============================================

%{Phalcon_Session_Adapter_5fb89a6d366cba76ef8e2c3e5a559b4f|:doc:`Phalcon\\Session\\AdapterInterface <Phalcon_Session_AdapterInterface>`}%

%{Phalcon_Session_Adapter_335244b9f0c94b283ea5dd14821685a9}%

%{Phalcon_Session_Adapter_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Session_Adapter_38d168e0be68a8fb5da8d35b168503c0}%

%{Phalcon_Session_Adapter_d936b77f9b5c6a0db4eb07f2649f0edf}%

%{Phalcon_Session_Adapter_4efdfc6db0248d6e3f98218f90fe9b22}%

%{Phalcon_Session_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_c567be28a4cc05d7aabf05700ca9c66c}%

%{Phalcon_Session_Adapter_e8c06940719da5c629a41b5a5f43b4b1}%

%{Phalcon_Session_Adapter_d3b1bb1b579590c4844ba7f6c905b393}%

%{Phalcon_Session_Adapter_b62d2bd5ef8cd029adf996cbc7c8e980}%

.. code-block:: php

    <?php

    $session->setOptions(array(
    	'uniqueId' => 'my-private-app'
    ));





%{Phalcon_Session_Adapter_8cd706d17906efae1c72cecaa708884f}%

%{Phalcon_Session_Adapter_5ddbfa842231257606ca4fdaecf60ffd}%

%{Phalcon_Session_Adapter_a7e10f1e18fa2a0de9f8015d263eb1c3}%

%{Phalcon_Session_Adapter_523152e8fe94432d635294daaa0edc15}%

%{Phalcon_Session_Adapter_6eba30f42f4a2188b2cd26a5960edc5c}%

%{Phalcon_Session_Adapter_18e84ad546d026883951245b0db9e254}%

.. code-block:: php

    <?php

    $session->set('auth', 'yes');





%{Phalcon_Session_Adapter_8606ff24cd6736acbcfc18ce86b1e817}%

%{Phalcon_Session_Adapter_28faca74b7da1a87deeca1ac7262d7d7}%

.. code-block:: php

    <?php

    var_dump($session->has('auth'));





%{Phalcon_Session_Adapter_1cf3744652b3480b78cd8862852c4351}%

%{Phalcon_Session_Adapter_039ab89230cc3863b7513a1cd3bc0782}%

.. code-block:: php

    <?php

    $session->remove('auth');





%{Phalcon_Session_Adapter_562a03593283995658bcadda5e45b719}%

%{Phalcon_Session_Adapter_dfe810a835e75dbc2161344390d6b06b}%

.. code-block:: php

    <?php

    echo $session->getId();





%{Phalcon_Session_Adapter_82b47b1c0ea967c6524ce1ee43a9becc}%

%{Phalcon_Session_Adapter_e395074048c4ddeb356b2b4b80c0293b}%

.. code-block:: php

    <?php

    var_dump($session->isStarted());





%{Phalcon_Session_Adapter_e833fb62e82b164ff32710510071e571}%

%{Phalcon_Session_Adapter_931e67f27415f11f2fe5ac39b9b7a849}%

.. code-block:: php

    <?php

    var_dump($session->destroy());





%{Phalcon_Session_Adapter_e53e39ef9124fdaf502df86665d52fe5}%

%{Phalcon_Session_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_0014d861097168e8f304d9a82973633a}%

%{Phalcon_Session_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_f8457b6e6bc7c32562b3138b58b22342}%

%{Phalcon_Session_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_19ed33479cdae693d8ef869f55426eb1}%

%{Phalcon_Session_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_f5a043ff4cdbf7e308abd0d2e36ff6cb}%

%{Phalcon_Session_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_2f89883b4ea5702517b8537bed8db873}%

%{Phalcon_Session_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_9ab68bc06a3129747a4a59466efb3f27}%

%{Phalcon_Session_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_3644e01a19440baa19219140adc68825}%

%{Phalcon_Session_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_dac5cec50eda033a74b25dcc1b83e1ba}%

%{Phalcon_Session_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_67283030de138d8819365798f72e21e3}%

%{Phalcon_Session_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_163bee86c090612c50ba84de65e12f98}%

%{Phalcon_Session_Adapter_5298b5913d6b953ed35394a5baf0eae6}%

