<?
if( $_FILES['file']['name'] != "" )
{
   echo copy( $_FILES['file']['name'], "/var/www/html" ) or 
           die( "Could not copy file!");
	
}
else
{
    die("No file specified!");
}

?>
<html>
<head>
<title>File Uploading Form</title>
</head>
<body>
<h3>File Upload:</h3>
Select a file to upload: <br />
<form action="#" method="post" enctype="multipart/form-data">
<input type="file" name="file" size="50" />
<br />
<input name='submit' type="submit" value="Upload File" />
</form>
<h2>Uploaded File Info:</h2>
<ul>
<li>Sent file: <?php echo $_FILES['file']['name'];  ?>
<li>File size: <?php echo $_FILES['file']['size'];  ?> bytes
<li>File type: <?php echo $_FILES['file']['type'];  ?>
</ul>
</body>
</html>