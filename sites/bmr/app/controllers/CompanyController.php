<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class CompanyController extends Controller
{
	public function initialize(){
		$this->view->setTemplateAfter('template');
	}


	public function addcompanyAction()
	{
		$this->view->setVar('header_title', "Add New Company");
		if ($this->request->isPost('save') == true) {
		 	//VARIABLE
		 	$compname= $this->request->getPost('compname');
		 	$address= $this->request->getPost('address') ;
		 	$contact= $this->request->getPost('contact') ;
		 	//ADD SAVE NEW DATA ENTRY
		 	$add = new Company();
		 	$add->campname 	= $compname;
		 	$add->address 	= $address;
		 	$add->contact 	= $contact;

		 	if ($add->save() == false) {
		 		echo "Umh, We can store data: ";
		 		foreach ($robot->getMessages() as $message) {
		 			echo $message;
		 		}
		 	} else {
		 		echo "Great, a new data was saved successfully!";
		 	}
        }
	}

	public function compListAction($num="",$page="",$keyword="")
	{
		$this->view->setVar('header_title', "Client Company List");

		if(!isset($_GET["page"])){
			$currentPage=0;
		}else{
			$currentPage = (int) $_GET["page"];
		}
		// The data set to paginate
		$company      = Company::find();

		// Create a Model paginator, show 10 rows by page starting from $currentPage
		$paginator   = new PaginatorModel(
			array(
				"data"  => $company,
				"limit" => 5,
				"page"  => $currentPage
				)
			);

		// Get the paginated results
		$this->view->page= $paginator->getPaginate();;


	}

}
