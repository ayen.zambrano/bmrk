%{Phalcon_Mvc_Model_Transaction_Manager_409d699d55896dbeaa088130069d1ab6}%
===================================================

%{Phalcon_Mvc_Model_Transaction_Manager_7b5434a346fad7a1f97fe16a708fecea|:doc:`Phalcon\\Mvc\\Model\\Transaction\\ManagerInterface <Phalcon_Mvc_Model_Transaction_ManagerInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_Model_Transaction_Manager_bb764b02561a5f2f5887023308e5ec3a}%

.. code-block:: php

    <?php

    try {
    
      use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
    
      $transactionManager = new TransactionManager();
    
      $transaction = $transactionManager->get();
    
      $robot = new Robots();
      $robot->setTransaction($transaction);
      $robot->name = 'WALL·E';
      $robot->created_at = date('Y-m-d');
      if($robot->save()==false){
        $transaction->rollback("Can't save robot");
      }
    
      $robotPart = new RobotParts();
      $robotPart->setTransaction($transaction);
      $robotPart->type = 'head';
      if($robotPart->save()==false){
        $transaction->rollback("Can't save robot part");
      }
    
      $transaction->commit();
    
    }
    catch(Phalcon\Mvc\Model\Transaction\Failed $e){
      echo 'Failed, reason: ', $e->getMessage();
    }




%{Phalcon_Mvc_Model_Transaction_Manager_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Transaction_Manager_1f8893fbb5649d8903d8a3b4cf2f1522|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Transaction_Manager_a648bd07ab54e919f78ab6911b812685}%

%{Phalcon_Mvc_Model_Transaction_Manager_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Transaction_Manager_992eb21f94076f486939b432849d799c}%

%{Phalcon_Mvc_Model_Transaction_Manager_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Transaction_Manager_1bac54934bec20940b708d05267cfd49}%

%{Phalcon_Mvc_Model_Transaction_Manager_44947eccbb20b4c84efadb82a881b034|:doc:`Phalcon\\Mvc\\Model\\Transaction\\Manager <Phalcon_Mvc_Model_Transaction_Manager>`}%

%{Phalcon_Mvc_Model_Transaction_Manager_7338d2fafbe1491032dd52f1be914fb3}%

%{Phalcon_Mvc_Model_Transaction_Manager_bd1fa14f45ff29b8eae48d7533670a13}%

%{Phalcon_Mvc_Model_Transaction_Manager_b06b0efe8a61630cf3ab1994e921e5e7}%

%{Phalcon_Mvc_Model_Transaction_Manager_53e1499f1215cad66b78147bc599ca82|:doc:`Phalcon\\Mvc\\Model\\Transaction\\Manager <Phalcon_Mvc_Model_Transaction_Manager>`}%

%{Phalcon_Mvc_Model_Transaction_Manager_7d4b82fa450efa08d9633109a6a03d89}%

%{Phalcon_Mvc_Model_Transaction_Manager_91a75042fcba0b655effb5fd2fc2b730}%

%{Phalcon_Mvc_Model_Transaction_Manager_f7756f914195c204f03ab61ed9b80b25}%

%{Phalcon_Mvc_Model_Transaction_Manager_c2f14fcd3fba68384f52dfad1f0e1c46}%

%{Phalcon_Mvc_Model_Transaction_Manager_5ba60faae6f13c2dd5dad500b33f7c03}%

%{Phalcon_Mvc_Model_Transaction_Manager_fcecd2ea70ea6530f8d0e02671eeeb12|:doc:`Phalcon\\Mvc\\Model\\TransactionInterface <Phalcon_Mvc_Model_TransactionInterface>`}%

%{Phalcon_Mvc_Model_Transaction_Manager_4740a385beeaba737e3a8e744b4062e8}%

%{Phalcon_Mvc_Model_Transaction_Manager_d262ce662fa0c5881148abd82b456a07|:doc:`Phalcon\\Mvc\\Model\\TransactionInterface <Phalcon_Mvc_Model_TransactionInterface>`}%

%{Phalcon_Mvc_Model_Transaction_Manager_ac3a3b58b78503e8227e0bbc4847c64e}%

%{Phalcon_Mvc_Model_Transaction_Manager_1c3797faa503723926107856182727a9}%

%{Phalcon_Mvc_Model_Transaction_Manager_c9270ea8c7997bbbbbfcd5914b4d7475}%

%{Phalcon_Mvc_Model_Transaction_Manager_c6e1b4d24bca89ae402349f7f7380840}%

%{Phalcon_Mvc_Model_Transaction_Manager_ae56056f15ec8a3f1dcdb9d1ce546ef0}%

%{Phalcon_Mvc_Model_Transaction_Manager_4ee161938cced6b07849b605925a470a}%

%{Phalcon_Mvc_Model_Transaction_Manager_526c8f422e9eed8d23f7301aadd46146}%

%{Phalcon_Mvc_Model_Transaction_Manager_2bf1a5122c04422f1d5eee6dd2e730b0|:doc:`Phalcon\\Mvc\\Model\\TransactionInterface <Phalcon_Mvc_Model_TransactionInterface>`}%

%{Phalcon_Mvc_Model_Transaction_Manager_28f095a3bff9b3a3022b05a22cbdd48d}%

%{Phalcon_Mvc_Model_Transaction_Manager_fd54d38b2390c1c3efba65053e1ba038|:doc:`Phalcon\\Mvc\\Model\\TransactionInterface <Phalcon_Mvc_Model_TransactionInterface>`}%

%{Phalcon_Mvc_Model_Transaction_Manager_f4393945bf555ad626fc97484392baf5}%

%{Phalcon_Mvc_Model_Transaction_Manager_af0d799a9d3bbfd924e0a4f0481eaf23}%

%{Phalcon_Mvc_Model_Transaction_Manager_259554d4498eaf7d7162c42769fac936}%

%{Phalcon_Mvc_Model_Transaction_Manager_518233f4024a85a4ab5b559aeaf751f1}%

%{Phalcon_Mvc_Model_Transaction_Manager_b88f6128dc523f9f11b5d95c806a6abc}%

