%{Phalcon_Paginator_Adapter_NativeArray_2b8b282930fda9722a1cd9bf9a41c557}%
==================================================

%{Phalcon_Paginator_Adapter_NativeArray_07d515ab63df29c1deb305348e631c5a|:doc:`Phalcon\\Paginator\\AdapterInterface <Phalcon_Paginator_AdapterInterface>`}%

%{Phalcon_Paginator_Adapter_NativeArray_ec92c4eba76f2a61a22b62a568260eb5}%

.. code-block:: php

    <?php

    $paginator = new \Phalcon\Paginator\Adapter\Model(
    	array(
    		"data"  => array(
    			array('id' => 1, 'name' => 'Artichoke'),
    			array('id' => 2, 'name' => 'Carrots'),
    			array('id' => 3, 'name' => 'Beet'),
    			array('id' => 4, 'name' => 'Lettuce'),
    			array('id' => 5, 'name' => '')
    		),
    		"limit" => 2,
    		"page"  => $currentPage
    	)
    );




%{Phalcon_Paginator_Adapter_NativeArray_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Paginator_Adapter_NativeArray_57ceaea4079c7ff3deb8a0d9cc79d9cb}%

%{Phalcon_Paginator_Adapter_NativeArray_7e42a9c7b4b7c565055ce25822d51985}%

%{Phalcon_Paginator_Adapter_NativeArray_521b5ca8048862791607a1117fb02e7b}%

%{Phalcon_Paginator_Adapter_NativeArray_bf13cfd2c677a3545b33c620323dc117}%

%{Phalcon_Paginator_Adapter_NativeArray_282e2f7ac198967ea80103a8eaa78efe}%

%{Phalcon_Paginator_Adapter_NativeArray_3df4a0a909a65cde86c9c9793c2a9d49}%

