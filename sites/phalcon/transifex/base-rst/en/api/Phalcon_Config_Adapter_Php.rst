%{Phalcon_Config_Adapter_Php_df1c8b5da4e1c89b26a67b78cb8c4d62}%
=======================================

%{Phalcon_Config_Adapter_Php_ea6c93ca1a7d7149d6682d7966a401a4|:doc:`Phalcon\\Config <Phalcon_Config>`}%

%{Phalcon_Config_Adapter_Php_fca1ea0b1fe3f9769124d410df005401}%

%{Phalcon_Config_Adapter_Php_901119d1c5ec2b56dfc5ff7f1679a8f1}%

.. code-block:: php

    <?php

    <?php
    return array(
    'database' => array(
    	'adapter' => 'Mysql',
    	'host' => 'localhost',
    	'username' => 'scott',
    	'password' => 'cheetah',
    	'dbname' => 'test_db'
    ),
    
    'phalcon' => array(
    	'controllersDir' => '../app/controllers/',
    	'modelsDir' => '../app/models/',
    	'viewsDir' => '../app/views/'
    ));

  You can read it as follows:  

.. code-block:: php

    <?php

    $config = new Phalcon\Config\Adapter\Php("path/config.php");
    echo $config->phalcon->controllersDir;
    echo $config->database->username;




%{Phalcon_Config_Adapter_Php_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Config_Adapter_Php_c044a39a61c4cca5faedb1dcc3867532}%

%{Phalcon_Config_Adapter_Php_711608968e708b9813e9d1d0ebcdb750}%

%{Phalcon_Config_Adapter_Php_ad184d301499efce8f3964ca6dc4bb3a}%

%{Phalcon_Config_Adapter_Php_cef9ef423d3226f1df89e6c695359390}%

.. code-block:: php

    <?php

     var_dump(isset($config['database']));





%{Phalcon_Config_Adapter_Php_3712567de59f17bfbe84e4a63effe296}%

%{Phalcon_Config_Adapter_Php_0b8c0846e2f4b6b32161721525431ec3}%

.. code-block:: php

    <?php

     echo $config->get('controllersDir', '../app/controllers/');





%{Phalcon_Config_Adapter_Php_250aecef2df720bcebe5f7cb2ae8bd78}%

%{Phalcon_Config_Adapter_Php_cb9d856c70df42bb2d3346be63f239cc}%

.. code-block:: php

    <?php

     print_r($config['database']);





%{Phalcon_Config_Adapter_Php_f2d8b16bf9da301de67f85a740661e95}%

%{Phalcon_Config_Adapter_Php_a8bce44be354ca47d470b0631b08fccf}%

.. code-block:: php

    <?php

     $config['database'] = array('type' => 'Sqlite');





%{Phalcon_Config_Adapter_Php_29871184433a14db720da0828895a713}%

%{Phalcon_Config_Adapter_Php_bee829db6cea0d7a64fd97770ff67610}%

.. code-block:: php

    <?php

     unset($config['database']);





%{Phalcon_Config_Adapter_Php_8f080354fc2bf1de28b68db6d7fd3c80|:doc:`Phalcon\\Config <Phalcon_Config>`}%

%{Phalcon_Config_Adapter_Php_cb6ab50e7931dd69b9e01ede416ae818}%

.. code-block:: php

    <?php

    $appConfig = new Phalcon\Config(array('database' => array('host' => 'localhost')));
    $globalConfig->merge($config2);





%{Phalcon_Config_Adapter_Php_82d6f2943519bac0b9df5c24b7205c04}%

%{Phalcon_Config_Adapter_Php_12680a2b6379e22d5147df5a8a4f0267}%

.. code-block:: php

    <?php

    print_r($config->toArray());





%{Phalcon_Config_Adapter_Php_807079f12d6081992cea44ccadb3a6fb}%

%{Phalcon_Config_Adapter_Php_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Php_8a1c48ad387accfb23e5b14ab8a2204b}%

%{Phalcon_Config_Adapter_Php_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Php_68f9c10a09334876e0988c8d46977b50|:doc:`Phalcon\\Config <Phalcon_Config>`}%

%{Phalcon_Config_Adapter_Php_b23e852f6995564547321fb333c04912}%

%{Phalcon_Config_Adapter_Php_17ab47f2caca58e3466bc9754b2378df}%

%{Phalcon_Config_Adapter_Php_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Php_99bec0673999e64b60b71ba5935a345b}%

%{Phalcon_Config_Adapter_Php_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Php_6b74253a88eb7ec2b979fd16b3c976ba}%

%{Phalcon_Config_Adapter_Php_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Php_45106ea4943377cec724075ab9030769}%

%{Phalcon_Config_Adapter_Php_68cf0a3bff1e41b907cf955f570c5249}%

