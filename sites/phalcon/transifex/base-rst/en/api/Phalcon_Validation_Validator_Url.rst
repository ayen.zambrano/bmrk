%{Phalcon_Validation_Validator_Url_4fdb5ad8d20f365753b8142906ea8bb0}%
=============================================

%{Phalcon_Validation_Validator_Url_cac0f4e36b6c7c0d12c2ea9fa8f05795|:doc:`Phalcon\\Validation\\Validator <Phalcon_Validation_Validator>`}%

%{Phalcon_Validation_Validator_Url_3bc33f5008c906bbf75c850085f3e576|:doc:`Phalcon\\Validation\\ValidatorInterface <Phalcon_Validation_ValidatorInterface>`}%

%{Phalcon_Validation_Validator_Url_66d2b5ff44fc64adfd602229dfa0728b}%

.. code-block:: php

    <?php

    use Phalcon\Validation\Validator\Url as UrlValidator;
    
    $validator->add('url', new UrlValidator(array(
       'message' => 'The url is not valid'
    )));




%{Phalcon_Validation_Validator_Url_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Validation_Validator_Url_11680e0816c2b5612e05aaf250906b3f|:doc:`Phalcon\\Validation <Phalcon_Validation>`}%

%{Phalcon_Validation_Validator_Url_d01ba9899fa892ec2b62154529dce37f}%

%{Phalcon_Validation_Validator_Url_083f8660fec7a850b97c924c6ec8b3b5}%

%{Phalcon_Validation_Validator_Url_ec865eb22d34722d5b3dfe1c2403678d}%

%{Phalcon_Validation_Validator_Url_81b7291a5b45df34e31addf4c20fb319}%

%{Phalcon_Validation_Validator_Url_13f58da2ee2f606006004d67f4b648cc}%

%{Phalcon_Validation_Validator_Url_b0f96a5c6a69977cb2d6d47e77611416}%

%{Phalcon_Validation_Validator_Url_6685002cddbd3be00d9dc4ed9cea3a75}%

%{Phalcon_Validation_Validator_Url_74181c24b3c3cfcd1f72f02a5f4b5ae3}%

%{Phalcon_Validation_Validator_Url_2e7772bfbad31074c960e8f21b33e650}%

