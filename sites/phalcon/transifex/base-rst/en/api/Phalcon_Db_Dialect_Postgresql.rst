%{Phalcon_Db_Dialect_Postgresql_896a49e8c0d751c978a4978e7fe1572e}%
==========================================

%{Phalcon_Db_Dialect_Postgresql_534c63b4994b43e2dd295ea6552e85c1|:doc:`Phalcon\\Db\\Dialect <Phalcon_Db_Dialect>`}%

%{Phalcon_Db_Dialect_Postgresql_63ac1a796a42ab6bf74eecef37fe105a|:doc:`Phalcon\\Db\\DialectInterface <Phalcon_Db_DialectInterface>`}%

%{Phalcon_Db_Dialect_Postgresql_d9088f1a2b54f6b311e3398949a0ed10}%

%{Phalcon_Db_Dialect_Postgresql_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Db_Dialect_Postgresql_1e2995e761c9d63f65abea294829b4df|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Dialect_Postgresql_ab73867e640bb1cd81d8a7288c448e50}%

%{Phalcon_Db_Dialect_Postgresql_6993bbfcb111f2c33dc6d3ae0953c463|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Dialect_Postgresql_25cfa707a466e80c5a1b79cab53e10d0}%

%{Phalcon_Db_Dialect_Postgresql_f0b9a405d1e53eef5bad251c2192e3fa|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Dialect_Postgresql_b058dfa0245ea7e8bd22985bea62b2c6}%

%{Phalcon_Db_Dialect_Postgresql_e22e98b7fcd124b0961a48c842d6f3e4}%

%{Phalcon_Db_Dialect_Postgresql_c7c8b45ab9e084096ba63b247cbb5394}%

%{Phalcon_Db_Dialect_Postgresql_6300626de6f32651764ff8b621cf158a|:doc:`Phalcon\\Db\\Index <Phalcon_Db_Index>`}%

%{Phalcon_Db_Dialect_Postgresql_b67c92d9bf1d1079045c7e4cee55a8af}%

%{Phalcon_Db_Dialect_Postgresql_5d091360cdc042680ca280a11151995f}%

%{Phalcon_Db_Dialect_Postgresql_b73e7e0ddb6af63bf574d341516f3b28}%

%{Phalcon_Db_Dialect_Postgresql_6249ec8641636932809aad0343660bbe|:doc:`Phalcon\\Db\\Index <Phalcon_Db_Index>`}%

%{Phalcon_Db_Dialect_Postgresql_dcf061f20dd16b8fec02dfc0e6b0cd85}%

%{Phalcon_Db_Dialect_Postgresql_62f527a68a5e65369c2a8431de3ef6ba}%

%{Phalcon_Db_Dialect_Postgresql_887d9e3d8ff92a764c34c4da28a8cfbf}%

%{Phalcon_Db_Dialect_Postgresql_07d44f90b363065f385a00177cf73b87|:doc:`Phalcon\\Db\\ReferenceInterface <Phalcon_Db_ReferenceInterface>`}%

%{Phalcon_Db_Dialect_Postgresql_b67c92d9bf1d1079045c7e4cee55a8af}%

%{Phalcon_Db_Dialect_Postgresql_043baa6805c256ef3d636850638c143b}%

%{Phalcon_Db_Dialect_Postgresql_524384868ab8de535006fce15c469bbe}%

%{Phalcon_Db_Dialect_Postgresql_f01966815470af1fc6edcf7161fb3646}%

%{Phalcon_Db_Dialect_Postgresql_7a5b52b55ef9cc1be002ee272896d361}%

%{Phalcon_Db_Dialect_Postgresql_6a12a1fd2d06aea5a4290597ede59ca7}%

%{Phalcon_Db_Dialect_Postgresql_54fd8c19f8812bb83e70458c12d01d30}%

%{Phalcon_Db_Dialect_Postgresql_56e462248c41f004760dfcda5e5fb08c}%

%{Phalcon_Db_Dialect_Postgresql_5ba9bf6b7553c075e551b79a056355bc}%

%{Phalcon_Db_Dialect_Postgresql_5e2c864e43282d5b3b39960a26470f39}%

%{Phalcon_Db_Dialect_Postgresql_0eb2b37d3b3bf3de62d1651b960afd7c}%

%{Phalcon_Db_Dialect_Postgresql_a9ea7981c2f4dbf84624db1bc4adb5fb}%

%{Phalcon_Db_Dialect_Postgresql_e958816ea7301c10ae61a3496f8ec523}%

%{Phalcon_Db_Dialect_Postgresql_eac5f83f82ca57a88484d52843fa25fb}%

%{Phalcon_Db_Dialect_Postgresql_12c05356450817f8d66bb9e9019c67f8}%

%{Phalcon_Db_Dialect_Postgresql_0d4f75e38e916631454afb4a73e53e87}%

%{Phalcon_Db_Dialect_Postgresql_1f3919bb87232f39dea5d14809509f6c}%

%{Phalcon_Db_Dialect_Postgresql_b27be46e1561141f769eff8247b0fb98}%

%{Phalcon_Db_Dialect_Postgresql_06e813c17ae6b868085ceeef99dda537}%

%{Phalcon_Db_Dialect_Postgresql_65715be9ff5d7304382d86644161dba8}%

%{Phalcon_Db_Dialect_Postgresql_6a143a0f07cc3295a76ff202795ab04f}%

.. code-block:: php

    <?php

    print_r($dialect->listTables("blog")) ?>





%{Phalcon_Db_Dialect_Postgresql_85f0633329bc102318ac47b0c9d170c2}%

%{Phalcon_Db_Dialect_Postgresql_64741d5750632bf3824bda82928cd08b}%

%{Phalcon_Db_Dialect_Postgresql_ce4c79a9a579a4bea579f191763826b9}%

%{Phalcon_Db_Dialect_Postgresql_0733e0f2aac399bbc7af53ddd9ab3f5e}%

%{Phalcon_Db_Dialect_Postgresql_c2fa6029efb7dad74ce745b8349e0d5c}%

%{Phalcon_Db_Dialect_Postgresql_f25e7cbd91858aa49a772a297227568f}%

%{Phalcon_Db_Dialect_Postgresql_ad0f53c49c69224a2fce7326d0bb6492}%

%{Phalcon_Db_Dialect_Postgresql_096c12d210232448b37199764a87a254}%

%{Phalcon_Db_Dialect_Postgresql_1d12db17ad9ee59b76b352bf260f092c}%

%{Phalcon_Db_Dialect_Postgresql_52d966c8fc82475765b525c9678b710b}%

.. code-block:: php

    <?php

     $sql = $dialect->limit('SELECT * FROM robots', 10);
     echo $sql; // {%Phalcon_Db_Dialect_Postgresql_ca03c429c7fcbcb5e0abe54197ccf503%}





%{Phalcon_Db_Dialect_Postgresql_c776da2ea4f7b5c63080c015f571e954}%

%{Phalcon_Db_Dialect_Postgresql_7002de3890fd2c005a97f19d1eafbd78}%

.. code-block:: php

    <?php

     $sql = $dialect->forUpdate('SELECT * FROM robots');
     echo $sql; // {%Phalcon_Db_Dialect_Postgresql_c581ef57ed270e78b1ab818a742dacf6%}





%{Phalcon_Db_Dialect_Postgresql_360b991d036771eb2610de1572cc1363}%

%{Phalcon_Db_Dialect_Postgresql_3581d9894b3b47e07a56f2a95f7ddbe9}%

.. code-block:: php

    <?php

     $sql = $dialect->sharedLock('SELECT * FROM robots');
     echo $sql; // {%Phalcon_Db_Dialect_Postgresql_247f3da6d75f482b27cc74d986547a3c%}





%{Phalcon_Db_Dialect_Postgresql_3863ada447f8dcfb7714a62a874f4999}%

%{Phalcon_Db_Dialect_Postgresql_bd1034394e086147db974565cd62c526}%

.. code-block:: php

    <?php

     echo $dialect->getColumnList(array('column1', 'column'));





%{Phalcon_Db_Dialect_Postgresql_0eeeefef7c76804b9dcbf7f932dbde88}%

%{Phalcon_Db_Dialect_Postgresql_89d2b02c18418315d930416bf30d7abc}%

%{Phalcon_Db_Dialect_Postgresql_6fce05da3bcd30f95371662da485d499}%

%{Phalcon_Db_Dialect_Postgresql_4a336b00cd8c57bdad466e8f7d1e085f}%

%{Phalcon_Db_Dialect_Postgresql_3e2b91a55983b60db34bd3ac3b51ea5d}%

%{Phalcon_Db_Dialect_Postgresql_920bf11618d4431c4cd50f652ad71973}%

%{Phalcon_Db_Dialect_Postgresql_4554df5fbb66b2b83939e04ef161654c}%

%{Phalcon_Db_Dialect_Postgresql_1565b67ca91624a4a4a823e8ed91dc9c}%

%{Phalcon_Db_Dialect_Postgresql_fbe9470602edfc80a30f514983fb8c94}%

%{Phalcon_Db_Dialect_Postgresql_f3ee491156b47a965d2f3e1245379ebb}%

%{Phalcon_Db_Dialect_Postgresql_4603a67b3a9f5b7feddd8bd51546d30f}%

%{Phalcon_Db_Dialect_Postgresql_e5ced1df78c13c5c62d1e58f7cfd63a1}%

%{Phalcon_Db_Dialect_Postgresql_23e52ddfa3819e82d6e577a2895840f5}%

%{Phalcon_Db_Dialect_Postgresql_1ed3c999236ff62f896692da89d43a51}%

%{Phalcon_Db_Dialect_Postgresql_74bef7f992140638b3775e096e9f32e9}%

%{Phalcon_Db_Dialect_Postgresql_6fbb66dee4faaf07d7914f9a5e1cb00e}%

